<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
			<!-- Slider Site -->
			<?php $sliders = get_field('slider_home', 'options'); ?>
			<div class="container-fluid" id="slider">
				<div class="slider height-mobile-300">
				    <ul class="slides height-mobile-300">
					    <?php foreach ($sliders as $slider) { ?>
				    	<li>
				        	<img class="responsive-img" src="<?php echo $slider['image']; ?>"/> 
							<div class="caption">
								<div class="space250 hide-on-small-only"></div>
								<div class="row">
									<div class="col s12 m12 l6">
										<div class="title">
											<a href="<?php echo $slider['link']; ?>">
												<h3 class="brandon font36 white-text bold uppercase truncate-slider-title"><span><?php echo $slider['title']; ?></span></h3>
												<h5 class="roboto font16 white-text justify truncate-slider"><span><?php echo $slider['description']; ?></span></h5>
											</a>
										</div>
									</div>
								</div>
								<div class="space40"></div>
				        	</div>
						</li>
						<?php } ?>
					</ul>
					<div class="news_button">
						<a href="<?php bloginfo('url'); ?>/noticias/">
							<h6 class="helvetica font14 white-text uppercase bold"><span><?php _e("Ver todas las noticias","acmx_v1"); ?></span></h6>
						</a>
					</div>
				</div>
			</div>
			
			<?php
				//Consulta de Artistas
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				/*$args = array(
					'post_type' 	   => 'artist',
					'posts_per_page'   => 6,
					'order_by' 		   => 'title',
					'order' 		   => 'ASC',
					'post_status'      => 'publish',
					'paged'			   => $paged,
					'suppress_filters' => false 
				);
				$query_artists = new WP_Query( $args );*/
				
				//Consulta de Lanzamientos
				$args = array(
					'post_type' 	   => 'release',
					'posts_per_page'   => 6,
					'order_by'	 	   => 'date',
					'order' 		   => 'DESC',
					'post_status'      => 'publish',
					'paged'			   => $paged,
					'suppress_filters' => false
				);
				$query_releases = new WP_Query( $args );
			?>
			
			<!-- Artist & Catalog -->
			<?php 
				//Leemos los Colores de Fondo
				$artists_color_start = get_field("artists_color_start", "option");
				$artists_color_end = get_field("artists_color_end", "option");
				$artists_background = '';
				
				//Construimos el Color de Fondo
				if ($artists_color_start) { $artists_background = 'background: ' . $artists_color_start . ';'; }
				if ($artists_color_start && $artists_color_end) { $artists_background = 'background: linear-gradient(to left bottom, ' . $artists_color_start . ', ' . $artists_color_end . ');'; }
			?>
			<div class="container-fluid no-margin-row" id="catalog"<?=($artists_background) ? ' style="' . $artists_background . '"' : ''; ?>>
				<div class="container">
					<div class="row" style="position: relative;">
					    <div class="col s12 m12 l4" id="catalog-artists">
					        <!-- Artists, Catalog, Ver Todos -->
					        <div class="space280 hide-on-med-and-down"></div>
					        <div class="section mobile-text-catalogo">
						        <span rel="artists" class="brandon font36 white-text active"><?php _e("ARTISTAS","acmx_v1"); ?></span>
					            <span rel="catalog" class="brandon font36 white-text"><?php _e("LANZAMIENTOS","acmx_v1"); ?></span>
					        </div>
					        <div class="space200 hide-on-med-and-down"></div>
					        <a href="<?php bloginfo("url"); ?>/catalogo" class="hide-on-med-only"><span class="roboto font18 white-text bold boton-mobile-todos-catalogo"><?php _e("VER TODOS","acmx_v1"); ?></span></a>
					        <div class="space100 hide-on-med-and-down"></div>
				    	</div>
						<div class="col s12 m12 l8 roboto mobile-margin60" id="catalog-table">
				        	<div class="space40"></div>
				        	<div class="space80 hide-on-med-and-down"></div>
				        	<div class="container text-center-container">
					        	<div class="responsive filters_letters">
									<a href="#!" filter="a" class="filter_letter">A</a></li>
									<a href="#!" filter="b" class="filter_letter">B</a></li>
									<a href="#!" filter="c" class="filter_letter">C</a></li>
									<a href="#!" filter="d" class="filter_letter">D</a></li>
									<a href="#!" filter="e" class="filter_letter">E</a></li>
									<a href="#!" filter="f" class="filter_letter">F</a></li>
									<a href="#!" filter="g" class="filter_letter">G</a></li>
									<a href="#!" filter="h" class="filter_letter">H</a></li>
									<a href="#!" filter="i" class="filter_letter">I</a></li>
									<a href="#!" filter="j" class="filter_letter">J</a></li>
									<a href="#!" filter="k" class="filter_letter">K</a></li>
									<a href="#!" filter="l" class="filter_letter">L</a></li>
									<a href="#!" filter="m" class="filter_letter">M</a></li>
									<a href="#!" filter="n" class="filter_letter">N</a></li>
									<a href="#!" filter="o" class="filter_letter">O</a></li>
									<a href="#!" filter="p" class="filter_letter">P</a></li>
									<a href="#!" filter="q" class="filter_letter">Q</a></li>
									<a href="#!" filter="r" class="filter_letter">R</a></li>
									<a href="#!" filter="s" class="filter_letter">S</a></li>
									<a href="#!" filter="t" class="filter_letter">T</a></li>
									<a href="#!" filter="u" class="filter_letter">U</a></li>
									<a href="#!" filter="v" class="filter_letter">V</a></li>
									<a href="#!" filter="w" class="filter_letter">W</a></li>
									<a href="#!" filter="x" class="filter_letter">X</a></li>
									<a href="#!" filter="y" class="filter_letter">Y</a></li>
									<a href="#!" filter="z" class="filter_letter">Z</a></li>
								</div>
				        	</div>
				        	<?php $query_artists = get_field("artists", "option"); $contador_artists = 0; $contador_contenedor_artists = 1; ?>
				        	<input type="hidden" id="lang_site" value="<?php echo ICL_LANGUAGE_CODE; ?>">
				        	<input type="hidden" id="active_section" value="artists">
				        	<input type="hidden" id="filter_artists" value="all">
				        	<input type="hidden" id="artists_page" value="<?php echo $contador_contenedor_artists; ?>">
				        	<input type="hidden" id="artists_max_page" value="<?php echo count($query_artists); ?>" />
				        	<input type="hidden" id="error_artists_max_page" value="<?php _e("Ya no hay más artistas por mostrar.","acmx_v1"); ?>" />
				        	<input type="hidden" id="filter_catalog" value="all">
				        	<input type="hidden" id="catalog_page" value="1">
				        	<input type="hidden" id="catalog_max_page" value="<?php echo $query_releases->max_num_pages; ?>" />
				        	<input type="hidden" id="error_catalog_max_page" value="<?php _e("Ya no hay más lanzamientos por mostrar.","acmx_v1"); ?>" />
				        	<div class="row container_artists">
					        	<div class="space40"></div>
					        	<div class="items_artists" id="artists_<?=$contador_contenedor_artists;?>">
				        			<?php foreach ($query_artists as $artist) { $contador_artists++; $image_artist = get_the_post_thumbnail_url( $artist->ID, $size = 'full' ); ?>
						        		<?php if ($contador_artists % 7 == 0) { $contador_contenedor_artists++; echo '</div><div class="items_artists" id="artists_'.$contador_contenedor_artists.'" style="display:none;">'; $contador_artists = 1; } ?>
						        		<div class="item_artist hoverable">
								        	<a href="<?php echo get_permalink($artist->ID); ?>">
									        	<img class="centered-and-cropped-home-catalog" src="<?php echo $image_artist; ?>"/>
												<span class="artist-title roboto font16 white-text"><?php echo $artist->post_title; ?></span>
											</a>
							        	</div>
						        	<?php } ?>
					        		<!--
				        			<?php foreach ($query_artists->posts as $artist) { $image_artist = get_the_post_thumbnail_url( $artist->ID, $size = 'full' ); ?>
						        		<div class="item_artist">
								        	<a href="<?php echo get_permalink($artist->ID); ?>">
									        	<img class="centered-and-cropped-home-catalog" src="<?php echo $image_artist; ?>"/>
												<span class="artist-title roboto font16 white-text"><?php echo $artist->post_title; ?></span>
											</a>
							        	</div>
						        	<?php } ?>	
						        	-->			        		
					        	</div>
					        	<div class="col s12 m12 l12 centered">
						        	<div class="space40"></div>
						        	<a href="#!" class="bt-bk"></a>
						        	<a href="#!" class="bt-fr"></a>
						        	<div class="space20"></div>
					        	</div>
				        	</div>
				        	<div class="row container_catalog">
					        	<div class="space40"></div>	
			        			<div class="items_releases">
			        				<?php foreach ($query_releases->posts as $release) { $image_artist = get_the_post_thumbnail_url( $release->ID, $size = 'full' ); ?>
									<div class="item_artist">
						        		<a href="<?php echo get_permalink($release->ID); ?>">
							        		<img class="centered-and-cropped-home-catalog" src="<?php echo $image_artist; ?>"/>
						        			<span class="artist-title roboto font16 white-text italic">
						        				<?php echo $release->post_title; ?>
						        				<br/>
						        				<?php //$release_date = get_field("release_date", $release->ID); ?>
						        				<?php //echo substr($release_date,0,4).'/'.substr($release_date,4,2).'/'.substr($release_date,6,2); ?>
						        			</span>
										</a>
					        		</div>
					        		<?php } ?>
			        			</div>
					        	<div class="col s12 m12 l12 centered">
						        	<div class="space40 hide-on-med-only"></div>
						        	<a href="#!" class="bt-bk hoverable" style="display: none;"></a>
						        	<a href="#!" class="bt-fr hoverable"></a>
						        	<div class="space20"></div>
					        	</div>
				        	</div>
				    	</div>
					</div>
				</div>
			</div>
			
			<!-- Playlist -->
			<div class="container-fluid no-margin-row" id="playlist">
				<div class="container">	
					<div class="row" style="position: relative;">
					    <div class="col s12 m12 l4" id="play-all">
					        <!-- Playlist -->
					        <div class="space300 hide-on-med-and-down"></div>
					        <span class="brandon font36 black-text inline no-padding" id="title"><?php _e("PLAYLISTS","acmx_v1"); ?></span>
					        <div class="space80 hide-on-med-and-down"></div>
					        <a href="<?php bloginfo("url"); ?>/catalogo"><span class="roboto font18 black-text bold inline no-padding boton-mobile-todos-catalogo"><?php _e("VER TODOS","acmx_v1"); ?></span></a>
					        <div class="space80 hide-on-med-and-down"></div>
				    	</div>
						<div class="col s12 m12 l8 roboto mobile-margin60" id="play-content">
				        <!-- Content  -->
					        <div class="space40 hide-on-med-and-down"></div>
					        <?php $playlists = get_field("playlists", "options"); $contador_playlists = 0; $contador_contenedor_playlists = 1; ?>
				        	<input type="hidden" id="lang_site" value="<?php echo ICL_LANGUAGE_CODE; ?>">
				        	<input type="hidden" id="playlists_page" value="<?php echo $contador_contenedor_playlists; ?>">
				        	<input type="hidden" id="playlists_max_page" value="<?php echo count($playlists); ?>" />
				        	<input type="hidden" id="error_playlists_max_page" value="<?php _e("Ya no hay más playlists por mostrar.","acmx_v1"); ?>" />
				        	<div class="col s12 m6 offset-m3 l2 offset-l10 contador-playlist">
						        <?php the_field("embeed_spotify", "options"); ?>
					        </div>
					        <div class="space120 hide-on-small-only"></div>	
					        <div class="row container_playlists">
					        	<div class="space40"></div>	
			        			<div class="items_playlists" id="playlists_<?=$contador_contenedor_playlists;?>">
			        				<?php foreach ($playlists as $playlist) { $contador_playlists++; ?>
			        				<?php if ($contador_playlists % 7 == 0) { $contador_contenedor_playlists++; echo '</div><div class="items_playlists" id="playlists_'.$contador_contenedor_playlists.'" style="display:none;">'; $contador_playlists = 1; } ?>
									<div class="item_playlist">
						        		<a href="<?php echo $playlist['link']; ?>" target="_blank">
							        		<img class="centered-and-cropped-home-catalog" src="<?php echo $playlist['imagen']; ?>"/>
										</a>
					        		</div>
					        		<?php } ?>
			        			</div>
					        	<div class="col s12 m12 l12 centered">
						        	<div class="space40"></div>
						        	<a href="#!" class="bt-bk hoverable"></a>
						        	<a href="#!" class="bt-fr hoverable"></a>
									<div class="space40"></div>
					        	</div>
					        	<div class="space40"></div>
					        </div>
				    	</div>
					</div>
				</div>
			</div>
			
			<!-- Videos -->
			<?php $videos = get_field("videos", "option"); $contador_video = 0; $contador_video_page = 1; ?>
			<?php 
				//Leemos los Colores de Fondo
				$videos_color_start = get_field("videos_color_start", "option");
				$videos_color_end = get_field("videos_color_end", "option");
				$videos_background = '';
				
				//Construimos el Color de Fondo
				if ($videos_color_start) { $videos_background = 'background: ' . $videos_color_start . ';'; }
				if ($videos_color_start && $videos_color_end) { $videos_background = 'background: linear-gradient(to left bottom, ' . $videos_color_start . ', ' . $videos_color_end . ');'; }
			?>
			<div class="container-fluid no-margin-row" id="videos" style="position: relative;<?=($videos_background) ? ' ' . $videos_background : ''; ?>">
				<div class="container">
					<div class="row">
					    <div class="col s12 m12 l4" id="vid-info">
					        <!-- Song Title, Description, Video -->
					        <div class="space100 hide-on-med-and-down"></div>
					        <div class="space40 hide-on-large-only"></div>
					        <span class="brandon font48 white-text inline no-padding text-center-width mobile-padding20"><?php _e("VIDEOS","acmx_v1"); ?></span>
					        <?php foreach ($videos as $video) { $contador_video++; $youtube_id = $video['youtube_id']; ?>
					        <div class="space20 hide-on-small-only"></div>
					        <span class="roboto font18 white-text bold inline no-padding video_artist-mobile" id="video_artist"><?php echo $video['artist']; ?></span>
					        <div class="space5 hide-on-small-only"></div>
					        <span class="roboto font18 white-text light inline no-padding video_title-mobile" id="video_title"><?php echo $video['title']; ?></span>
					        <div class="space10 hide-on-small-only"></div>
					        <div class="divider-pos show-on-small">
					        <div class="divider vid-div"></div>
					        </div>
					        <h5><span class="roboto font16 white-text light no-padding hide-on-small-only" id="video_description"><?php echo $video['description']; ?></span></h5>
					        <div class="space60 hide-on-med-and-down"></div>
					        <?php if ($contador_video == 1) { break; } } ?>
					        <span class="roboto font18 white-text bold inline no-padding show-on-small boton-mobile-todos-catalogo"><?php _e("VER TODOS","acmx_v1"); ?></span>
					        <!--<div class="space80 show-on-small"></div>-->
				    	</div>
						<div class="col s12 m12 l8">
				        <!-- Content  -->
				        	<div class="space100 hide-on-med-and-down"></div>
				        	<div class="row video-play">
					        	<div class="video-container" id="vid-show">
					        		<iframe src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allowfullscreen></iframe>
					        	</div>
				        	</div>
				        	<div class="row mobile-margin90" class="video_pages">
					        	<div class="space40"></div>
					        	<div class="space90 show-on-small"></div>
					        	<div id="video_page_<?php echo $contador_video_page; ?>">
						        	<?php $contador_video = 0; ?>
						        	<?php foreach ($videos as $video) { $contador_video++; ?>
						        	<div class="col s4 m3 l3 video" youtube_id='<?php echo $video['youtube_id']; ?>' title='<?php echo $video['title']; ?>' artist='<?php echo $video['artist']; ?>' description='<?php echo $video['description']; ?>'>
							        	<img src="https://img.youtube.com/vi/<?php echo $video['youtube_id']; ?>/hqdefault.jpg" class="responsive-img"/>
							        	<div class="space20"></div>
							        	<span class="roboto bold font14 white-text text-center-block"><?php echo $video['title']; ?></span><br class="hide-on-small-only">
							        	<span class="roboto font14 white-text text-center-block"><?php echo $video['artist']; ?></span>
						        	</div>
						        	<?php if ($contador_video == 3) { $contador_video_page++; $contador_video = 0; echo '</div><div id="video_page_'.$contador_video_page.'" style="display:none;">'; } ?>
						        	<?php } ?>
						        </div>
						        <input type="hidden" id="pages" name="pages" value="<?php echo $contador_video_page; ?>" />
						        <input type="hidden" id="current_page" name="current_page" value="1" />
						        <?php if ($contador_video_page > 1) { ?>
					        	<div class="col4 m3 l3 centered hide-on-small-only">
						        	<a href="#!" class="bt-bk hoverable" style="display: none;"></a>
						        	<a href="#!" class="bt-fr hoverable"></a>
					        	</div>
					        	<?php } ?>
				        	</div>
				    	</div>
					</div>
				</div>
			</div>
			
			<!-- Tienda -->
			<!--
			<div class="container-fluid no-margin-row" id="store" style="position: relative;">
				<div class="container">
					<div class="row">
					    <div class="col s12 m12 l2" id="store-all">
					        <div class="space350 hide-on-med-and-down"></div>
					        <span class="brandon font48 black-text inline no-padding text-center-width mobile-padding20"><?php _e("TIENDA","acmx_v1"); ?></span>
					        <div class="space80 hide-on-med-and-down"></div>
					        <a class="roboto font18 black-text bold inline no-padding boton-mobile-todos-catalogo" href="<?php bloginfo("url"); ?>/tienda/"><?php _e("VER TODOS","acmx_v1"); ?></a>
					        <div class="space100 hide-on-med-and-down"></div>
				    	</div>
				    	<div class="products">
							<div class="col s12 m12 l9" id="sell-product">
							<?php
								//Consultamos los CDS
								$args = array(
									'posts_per_page'   => 6,
									'orderby'          => 'date',
									'order'            => 'DESC',
									'post_type'        => 'product',
									'post_status'      => 'publish',
									'suppress_filters' => false,
									'meta_query' => array(
										array(
											'key'     => 'type',
											'value'   => 'cd',
											'compare' => '=',
										),
									),
								);
								$query = new WP_Query( $args );
								$contador = 0;
								$renglon_desktop = 1;
							?>
				        	<div class="space120 hide-on-med-and-down"></div>
				        	<div class="hide-on-small-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s6 m3 l3 <?=($contador==1) ? 'offset-l1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 3) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-small-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s6 m3 l3 <?=($contador==1) ? 'offset-m1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 3) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s6 m3 l3 <?=($contador==1) ? 'offset-s' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32 hide-on-small-only"></div>
									        	<?php } ?>
									        	<div class="space10 hide-on-small-only"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 2) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
				    	</div>
				    	</div>
					</div>
				</div>
			</div>
			-->
			
			<!-- Calendario -->
			<?php $events_calendar = get_field('events_calendar', 'options'); //Leemos los Eventos ?>
			<?php $contador_eventos = 0; $events = array(); $year = date('Y'); $month = date('m'); $day = date('d'); $today = (string)$year.(string)$month.(string)$day; ?>
			<?php foreach ($events_calendar as $event) { if ((string)$event['date'] >= (string)$today) { $events[] = $event; } } // Procesamos las fechas validas ?>
			<div class="container-fluid no-margin-row" id="calendar">
				<div class="row">
				    <div class="col s12 m12 l5 offset-l1" id="date" style="position: relative;">
				        <!-- Playlist -->
				        <div class="space40 hide-on-small-only"></div>
				        <span class="brandon font48 black-text inline no-padding text-center-width"><?php _e("CALENDARIO","acmx_v1"); ?></span>
				        <div class="space20"></div>
				        <?php foreach ($events as $row_event) { ?>
				        	<?php $contador_eventos++; if ($contador_eventos > 5) { break; } ?>
				        	<?php if ($contador_eventos == 1) { $poster = $row_event['poster']; } ?>
				        	<?php $fecha = $row_event['date']; $dia = substr($fecha, 6, 2); $mes = substr($fecha, 4, 2); $mes_string = ''; ?> 
				        	<?php
					        	switch ($mes)
					        	{
						        	case '01': $mes_string = 'ENE'; break;
						        	case '02': $mes_string = 'FEB'; break;
						        	case '03': $mes_string = 'MAR'; break;
						        	case '04': $mes_string = 'ABR'; break;
						        	case '05': $mes_string = 'MAY'; break;
						        	case '06': $mes_string = 'JUN'; break;
						        	case '07': $mes_string = 'JUL'; break;
						        	case '08': $mes_string = 'AGO'; break;
						        	case '09': $mes_string = 'SEP'; break;
						        	case '10': $mes_string = 'OCT'; break;
						        	case '11': $mes_string = 'NOV'; break;
						        	case '12': $mes_string = 'DIC'; break;
						        	default: $mes_string = 'ENE'; break;
					        	} 
					        ?>
					        <div class="divider"></div>
							<div class="row event" rel="<?php echo $row_event['poster']; ?>">
								<div class="space20"></div>
								<div class="col s2 m2 l2 roboto font24 bold centered date font17-mobile"><span><?php echo $dia; ?><br/><?php echo $mes_string; ?></span></div>
								<div class="col s5 m6 l5 roboto font24 bold uppercase block font14-mobile">
									<span id="artist"><?php echo $row_event['artist']; ?></span>
									<span class="roboto font14 bold magnesium-text block font10-mobile" id="event"><?php echo $row_event['event']; ?></span>
									<span class="roboto font14 magnesium-text block font10-mobile" id="place"><?php echo $row_event['place']; ?></span>
								</div>
								<div class="col s4 m3 l4">
									<input type="submit" id="btbuy-e" rel="<?php echo $row_event['tickets']; ?>" class="right roboto font16 inline font10-mobile-400" value="<?php _e("Comprar Boletos","acmx_v1"); ?>">
								</div>
							</div>
						<?php } ?>
						<div class="divider"></div>
						<div class="space80"></div>
						<a href="<?php bloginfo("url"); ?>/en-vivo">
					        <span class="roboto font18 black-text bold inline no-padding boton-mobile-todos-catalogo"><?php _e("VER TODOS","acmx_v1"); ?></span>
						</a>
			    	</div>
					<div class="col s12 m12 l6" id="artist-tour">
			        <!-- Content  -->
			        	<div class="space120 hide-on-med-and-down"></div>
			        	<div class="col s12 m9 l9 hide-on-med-and-down">
					        <img class="materialboxed responsive-img poster" src="<?php echo $poster; ?>"/>
				        </div>
				        <!--
				        <div class="col s12 m12 l9">
					        <span class="roboto bold font18 black-text inline newsletter text-center-width"><?php _e("NEWSLETTER","acmx_v1"); ?></span>
				        </div>
				        <div class="input-field col s12 m12 l6 inline mobile-margin60">
					       	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
							<style type="text/css">
								#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
								input#mce-EMAIL.email { text-indent: 0px !important; }
							</style>
							<div id="mc_embed_signup">
								<form style="padding: 0px;" action="https://arts-crafts.us1.list-manage.com/subscribe/post?u=eda763f35af955af8a0f60dff&amp;id=94fc15a764" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								    <div id="mc_embed_signup_scroll">								
										<input style="text-indent: 10px; border: 0; height: 46px; margin-bottom: 1em; width: 100%;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php _e("Correo Electrónico","acmx_v1"); ?>" required>
									    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_eda763f35af955af8a0f60dff_94fc15a764" tabindex="-1" value=""></div>
									    <div class="clear">
										    <input style="background-color: #000; color: #fff; margin-top: -1px; border: 0; width: 100%; height: 63px; border-radius: 0px; font-family: roboto; font-size: 18px;" type="submit" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="mobile-input button">
										</div>
								    </div>
								</form>
							</div>
				        </div>
				        -->
			    	</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="space20"></div>
					</div>
				</div>
			</div>

<?php get_footer(); ?>