<?php

	//Remove Enqueue Scripts
	remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page();

	}
	
	add_filter('acf/settings/default_language', 'my_acf_settings_default_language');
 
	function my_acf_settings_default_language( $language ) {
	 
	    return 'en';
	    
	}
	
	add_filter('acf/settings/current_language', 'my_acf_settings_current_language');
 
	function my_acf_settings_current_language( $language ) {
	 
	    return 'es';
	    
	}
	
	//Register Menus
	function register_my_menus() {
	  register_nav_menus(
	    array(
	      'site-menu' => __( 'Site Menu' )
	    )
	  );
	}
	add_action( 'init', 'register_my_menus' );
	
	//Custom Site Menu
	function create_site_menu( $theme_location ) 
	{
		//Check Theme Location
	    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) 
	    {
		    //Check Languages
		    $languages = icl_get_languages('skip_missing=1'); 
		    $es = isset($languages['es']['url']) ? $languages['es']['url'] : '#'; 
		    $en = isset($languages['en']['url']) ? $languages['en']['url'] : '#';
		    
		    //Buit Navbar
	        $menu_list  = '<div class="navbar-fixed">' ."\n";
	        $menu_list .= '	<nav class="white menu">' ."\n";
	        $menu_list .= '		<div class="nav-wrapper container-fluid">' ."\n";
	        
	        //Add Logo
	        $menu_list .= '			<a href="' . get_bloginfo('url').'" class="brand-logo">' . "\n";
	        $menu_list .= '				<img src="' . get_bloginfo("template_directory") . '/img/Logo-Arts.svg"/>' ."\n";
	        $menu_list .= '			</a>' ."\n";
	        
	        //Add Mobile Menu Button
	        $menu_list .= '			<a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>' ."\n";
	        
	        //Read Menu Items 
	        $menu = get_term( $locations[$theme_location], 'nav_menu' );
	        $menu_items = wp_get_nav_menu_items($menu->term_id);
	 
			//Start Menu
	        $menu_list .= '			<ul id="nav-mobile" class="left hide-on-med-and-down">' ."\n";
	        
	        //Proccess Menu
	        foreach( $menu_items as $menu_item ) 
	        {
		        $bool = false;
		        
		        //Check Just Parent Items
	            if( $menu_item->menu_item_parent == 0 ) 
	            {   
		            //Assign Parent ID & Create Menu Array
	                $parent = $menu_item->ID;
	                $menu_array = array(); 
	                
	                //Proccess Items Menu
	                foreach( $menu_items as $submenu ) 
	                {
		                //Check if Item is Parent
	                    if( $submenu->menu_item_parent == $parent ) 
	                    {
		                    //Flag Submenu
	                        $bool = true;
	                        
	                        //Build Submenu Item
	                        $menu_array[] = '				<li><a class="brandon font14 black-text" target="_blank" href="'.$submenu->url.'">'.$submenu->title.'</a></li>' ."\n";
	                    }
	                }
	                
	                //Check If Item has a Submenu
	                if( $bool == true && count( $menu_array ) > 0 ) 
	                {
		                //Build Menu Item
	                	$menu_list .= '				<li><a class="din font14 dropdown-button" data-activates="'.sanitize_title($menu_item->title).'-hover" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
	                    
	                    //Add Submenu Items
	                    $menu_list .= '<ul id="'.sanitize_title($menu_item->title).'-hover" class="dropdown-content">' ."\n";
	                    $menu_list .= implode( "\n", $menu_array );
	                    $menu_list .= '</ul>' ."\n";
	                     
	                } 
	                else 
	                {
		                if (strtolower($menu_item->title) != 'store')
		                {
			                //Build Menu Item without Submenu
		                    $menu_list .= '				<li><a class="brandon font14 black-text" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
		                }
		                else
		                {
			                //Check Kichink Store ID
					        if (get_field("kichink_site", "option"))
					        {
						    	//Build Menu Item without Submenu
								$menu_list .= '				<li><a class="brandon font14 black-text" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>' ."\n";
						    }
		                }
	                }
	                 
	            }
	        }
	        
	        //End Menu
	        $menu_list .= '				</ul>';
	        
	        //Cart Menu
	        $menu_list .= '			<ul id="cart-mobile" class="right">' ."\n";
	        
	        //Check Kichink Store ID
	        if (get_field("kichink_site", "option"))
	        {
		        //Add Shopping Cart
		        //$menu_list .= '				<a href="#" class="din font22 inline btn-shopping"><svg id="shopping" data-name="shopping" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.6 45.5"><title>car</title><path d="M49.6,11.1H20.1a1.2,1.2,0,0,0-1.2,1.7l4.7,13.7a2,2,0,0,0,1.8,1.3H44.7a2,2,0,0,0,1.8-1.3l4.1-14A1,1,0,0,0,49.6,11.1Z" transform="translate(0 -2.6)"/><circle cx="22.7" cy="41" r="4.4"/><circle cx="41.4" cy="41.1" r="4.4"/><path d="M46.7,32.3H20.9L10.5,2.6h-8a2.5,2.5,0,1,0,0,5H7L17.3,37.3H46.7a2.5,2.5,0,1,0,0-5Z" transform="translate(0 -2.6)"/></svg> | 2</a>';
				$menu_list .= '				<span id="kichink-shoppingkart"></span>';
			}
				
	        //End Social Icons
	        $menu_list .= '					</li>';
	        
	        //Close Navbar
	        $menu_list .= '			</ul>' ."\n";
	        

	        //Start Social Icons
	        $menu_list .= '			<ul id="social-mobile" class="right hide-on-med-and-down">' ."\n";
	        
			$menu_list .= '					<li class="brandon font14 social">';
	        
	        //Add Facebook
	        if (get_field("facebook_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("facebook_link", "option").'" target="_blank" class="font14 inline"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.5 293.7"><title>fb</title><path d="M99,293.7V159.7h45l6.7-52.2H99V74.2c0-15.1,4.2-25.4,25.9-25.4h27.6V2.1A370,370,0,0,0,112.2,0C72.4,0,45.1,24.3,45.1,69v38.5H0v52.2H45.1V293.7Z"/></svg></a>';
	        }
	        
	        //Add Twitter
	        if (get_field("twitter_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("twitter_link", "option").'" target="_blank" class="font14 inline"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 202.3 164.4"><title>tw</title><path d="M202.3 19.5a83 83 0 0 1-23.9 6.5 41.6 41.6 0 0 0 18.3-23 83 83 0 0 1-26.3 10.1 41.5 41.5 0 0 0-70.8 37.8A117.8 117.8 0 0 1 14.1 7.6 41.5 41.5 0 0 0 26.9 63a41.3 41.3 0 0 1-18.8-5.2v.5A41.5 41.5 0 0 0 41.4 99a41.6 41.6 0 0 1-18.7.7 41.5 41.5 0 0 0 38.8 28.8 83.3 83.3 0 0 1-51.6 17.8 84.1 84.1 0 0 1-9.9-.6 117.4 117.4 0 0 0 63.6 18.6c76.3 0 118.1-63.2 118.1-118.1q0-2.7-.1-5.4a84.3 84.3 0 0 0 20.7-21.3z"/></svg></a>';
	        }
	        
	        //Add Instagram
	        if (get_field("instagram_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("instagram_link", "option").'" target="_blank" class="font14 inline"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.2 43.2"><title>in</title><path d="M14.4 21.6a7.2 7.2 0 1 1 7.2 7.2 7.2 7.2 0 0 1-7.2-7.2m-3.9 0a11.1 11.1 0 1 0 11.1-11.1 11.1 11.1 0 0 0-11.1 11.1m20-11.5a2.6 2.6 0 1 0 2.6-2.6 2.6 2.6 0 0 0-2.6 2.6M12.9 39.2a11.9 11.9 0 0 1-4-.7 6.7 6.7 0 0 1-2.5-1.6 6.7 6.7 0 0 1-1.6-2.5 12 12 0 0 1-.7-4c-.1-2.3-.1-3-.1-8.7s0-6.4.1-8.7a12 12 0 0 1 .7-4 6.7 6.7 0 0 1 1.6-2.6 6.7 6.7 0 0 1 2.5-1.6 11.9 11.9 0 0 1 4-.7h17.4a12 12 0 0 1 4 .7 6.7 6.7 0 0 1 2.5 1.6 6.7 6.7 0 0 1 1.6 2.5 12 12 0 0 1 .7 4c.1 2.3.1 3 .1 8.7s0 6.4-.1 8.7a12 12 0 0 1-.7 4 7.2 7.2 0 0 1-4.1 4.1 11.9 11.9 0 0 1-4 .7H12.9m-.2-39a15.8 15.8 0 0 0-5.2 1 10.6 10.6 0 0 0-3.9 2.5 10.6 10.6 0 0 0-2.5 3.8 15.8 15.8 0 0 0-1 5.2C0 15 0 15.7 0 21.6s0 6.6.1 8.9a15.8 15.8 0 0 0 1 5.2 10.6 10.6 0 0 0 2.5 3.8A10.6 10.6 0 0 0 7.4 42a15.8 15.8 0 0 0 5.2 1h17.9a15.8 15.8 0 0 0 5.2-1 11 11 0 0 0 6.3-6.3 15.8 15.8 0 0 0 1-5.2c.1-2.3.1-3 .1-8.9s0-6.6-.1-8.9a15.8 15.8 0 0 0-1-5.2 10.6 10.6 0 0 0-2.5-3.8 10.6 10.6 0 0 0-3.8-2.5 15.8 15.8 0 0 0-5.2-1H12.7"/></svg></a>';
	        }
	        
	        //Add Youtube
	        if (get_field("youtube_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("youtube_link", "option").'" target="_blank" class="font14 inline"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1021.4 718.4"><defs><style>.cls-1{fill-rule:evenodd;opacity:.12}</style></defs><title>yt</title><path id="The_Sharpness" data-name="The Sharpness" class="cls-1" d="M407 206l242 161.6 34-17.6z" transform="translate(-1.8 -1.3)"/><path d="M1013 156.3s-10-70.4-40.6-101.4C933.6 14.2 890 14 870.1 11.6 727.1 1.3 512.7 1.3 512.7 1.3h-.4s-214.4 0-357.4 10.3C135 14 91.4 14.2 52.6 54.9 22 85.9 12 156.3 12 156.3S1.8 238.9 1.8 321.6v77.5C1.8 481.8 12 564.4 12 564.4s10 70.4 40.6 101.4c38.9 40.7 89.9 39.4 112.6 43.7 81.7 7.8 347.3 10.3 347.3 10.3s214.6-.3 357.6-10.7c20-2.4 63.5-2.6 102.3-43.3 30.6-31 40.6-101.4 40.6-101.4s10.2-82.7 10.2-165.3v-77.5c0-82.7-10.2-165.3-10.2-165.3zM407 493V206l276 144z" transform="translate(-1.8 -1.3)" id="Lozenge"/></svg></a>';
	        }
	        
	        //Add Spotify
	        if (get_field("spotify_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("spotify_link", "option").'" target="_blank" class="font14 inline"><i class="fa fa-spotify black-text font20 inline" aria-hidden="true"></i></a>';
	        }
	        
	        //Add Newsletter
	        if (get_field("newsletter_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("newsletter_link", "option").'" target="_blank" class="font14 inline"><i class="fa fa-envelope black-text font20 inline" aria-hidden="true"></i></a>';
	        }
	        
	        //Add Search
	        $menu_list .= '				<a href="#search-modal" id="btnSearch" class="font14 inline modal-trigger"><i class="material-icons social-icon">search</i></a>';
	        
	        //Add Language Links
	        $menu_list .= '				<li class="leng-esp-eng brandon font14"><a class="brandon font16 black-text inline no-padding" href="'.$es.'">ESP</a> / <a class="brandon font16 black-text inline no-padding" href="'.$en.'">ENG</a></li>';
	        
	        //Close Navbar
	        $menu_list .= '			</ul>' ."\n";

	        
	        //Mobile Menu
	        $menu_list .= '			<ul class="side-nav black" id="mobile-menu">';
	        
	        //Search Input
	        $menu_list .= '				<li class="brandon font22 white-text"><div class="input-field block"><i class="material-icons prefix white-text">search</i><input id="icon_prefix" type="text" class="validate" placeholder="'.__("Buscar...","acmx_v1").'"><input type="hidden" id="error_search" name="error_search" value="'.__("Escribe un término a buscar...","acmx_v1").'"></div></li>';
			
			//Menu Items
			foreach( $menu_items as $menu_item ) 
			{
				$menu_list .= '				<li><a class="brandon font16 white-text" href="'.$menu_item->url.'">'.$menu_item->title.'</a></li>';
			}
			
			$menu_list .= '				<li class="brandon font24 social">';
			//Add Facebook
	        if (get_field("facebook_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("facebook_link", "option").'" target="_blank" class="brandon font14 inline"><i class="fa fa-facebook white-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>';
	        }
	        //Add Twitter
	        if (get_field("twitter_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("twitter_link", "option").'" target="_blank" class="brandon font14 inline"><i class="fa fa-twitter white-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>';
	        }
	        
	        //Add Instagram
	        if (get_field("instagram_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("instagram_link", "option").'" target="_blank" class="brandon font14 inline"><i class="fa fa-instagram white-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>';
	        }
	        
	        //Add Youtube
	        if (get_field("youtube_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("youtube_link", "option").'" target="_blank" class="brandon font14 inline"><i class="fa fa-youtube-play white-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>';
	        }
	        
	        //Add Spotify
	        if (get_field("spotify_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("spotify_link", "option").'" target="_blank" class="brandon font14 inline"><i class="fa fa-spotify white-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>';
	        }
	        
	        //Add iTunes
	        if (get_field("newsletter_link", "option")) 
	        {
	        	$menu_list .= '				<a href="'.get_field("newsletter_link", "option").'" target="_blank" class="brandon font14 inline"><i class="fa fa-envelope white-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>';
	        }
	        
	        $menu_list .= '				</li>';
			
			//Add Language Links
	        $menu_list .= '				<li class="brandon font22 white-text"><a class="brandon font16 inline no-padding white-text" href="'.$es.'">ESP</a> / <a class="brandon font16 inline no-padding white-text" href="'.$en.'">ENG</a></li>';     
			
			$menu_list .= '			</ul>';
			
	        $menu_list .= '		</div>' ."\n";
	        $menu_list .= '	</nav>' ."\n";
	        $menu_list .= '</div>' ."\n";
	    } 
	    else 
	    {
		    //Menu does not exists
	        $menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
	    }
	     
	    echo $menu_list;
	}
	
	add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
	add_filter('previous_posts_link_attributes', 'posts_link_attributes_back');
	
	function posts_link_attributes_back() {
	    return 'class="bt-bk"';
	}
	
	function posts_link_attributes_next() {
		return 'class="bt-fr"';
	}

	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) {
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Arts & Crafts México</i>';
	}
	function my_footer_version() {
	    return 'Version 1.0';
	}

	// PÃ¡ginas de ConfiguraciÃ³n
	add_filter('acf/options_page/settings', 'my_options_page_settings');

	function my_options_page_settings ( $options )
	{
		$options['title'] = __('Configuración');
		$options['pages'] = array(
			__('Site'),
			__('Footer')
		);

		return $options;
	}

	/* DefiniciÃ³n de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );

	/* Enqueue scripts and styles. */
	function scripts() {
		// Load CSS
		wp_enqueue_style( 'fontMaterialDesign', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		wp_enqueue_style( 'kichink', 'https://www.kichink.com/v2/themes/css/shoppingcart.css' );
		wp_enqueue_style( 'styles', CSSPATH . 'app.css', array(), '1.0.81' );
		// Load JS
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', JSPATH . 'app.js', array(), '1.0.89', false );
		wp_enqueue_script('kichink', 'https://www.kichink.com/v2/themes/js/shoppingkart.js', array('jquery'), '1.0.6', true );
	}
	add_action( 'wp_enqueue_scripts', 'scripts' );
	
	//FIX DATE MEXICO
	add_filter('the_time', 'modify_date_format');
	function modify_date_format(){
	    $month_names = array(1=>'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	    return get_the_time('j').' de '.$month_names[get_the_time('n')].' de '.get_the_time('Y');
	}

	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	function codex_custom_init() {

		//Artistas
		$labels = array(
		    'name' => _x('Artistas', 'post type general name'),
		    'singular_name' => _x('Artista', 'post type singular name'),
		    'add_new' => _x('Agregar Artista', 'New'),
		    'add_new_item' => __('Agregar Nuevo Artista'),
		    'edit_item' => __('Editar Artista'),
		    'new_item' => __('Nuevo Artista'),
		    'all_items' => __('Todos los Artistas'),
		    'view_item' => __('Ver Artista'),
		    'search_items' => __('Buscar Artistas'),
		    'not_found' =>  __('Artista no encontrado'),
		    'not_found_in_trash' => __('Artista no encontrado en papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Artistas'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-star-filled',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		);
		register_post_type('artist',$args);
		
		//Lanzamiento
		$labels = array(
		    'name' => _x('Lanzamientos', 'post type general name'),
		    'singular_name' => _x('Lanzamiento', 'post type singular name'),
		    'add_new' => _x('Agregar Lanzamiento', 'New'),
		    'add_new_item' => __('Agregar Nuevo Lanzamiento'),
		    'edit_item' => __('Editar Lanzamiento'),
		    'new_item' => __('Nuevo Lanzamiento'),
		    'all_items' => __('Todos los Lanzamientos'),
		    'view_item' => __('Ver Lanzamiento'),
		    'search_items' => __('Buscar Lanzamientos'),
		    'not_found' =>  __('Lanzamiento no encontrado'),
		    'not_found_in_trash' => __('Lanzamiento no encontrado en papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Lanzamientos'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-format-audio',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		);
		register_post_type('release',$args);
		
		//Productos
		$labels = array(
		    'name' => _x('Productos', 'post type general name'),
		    'singular_name' => _x('Producto', 'post type singular name'),
		    'add_new' => _x('Agregar Producto', 'New'),
		    'add_new_item' => __('Agregar Nuevo Producto'),
		    'edit_item' => __('Editar Producto'),
		    'new_item' => __('Nuevo Producto'),
		    'all_items' => __('Todos los Productos'),
		    'view_item' => __('Ver Producto'),
		    'search_items' => __('Buscar Productos'),
		    'not_found' =>  __('Producto no encontrado'),
		    'not_found_in_trash' => __('Producto no encontrado en papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Productos'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-cart',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		);
		register_post_type('product',$args);
		
		//Noticias
		$labels = array(
		    'name' => _x('Noticias', 'post type general name'),
		    'singular_name' => _x('Noticia', 'post type singular name'),
		    'add_new' => _x('Agregar nueva noticia', 'New'),
		    'add_new_item' => __('Agregar nueva noticia'),
		    'edit_item' => __('Editar Noticia'),
		    'new_item' => __('Nueva Noticia'),
		    'all_items' => __('Todas las Noticias'),
		    'view_item' => __('Ver Noticia'),
		    'search_items' => __('Buscar Noticia'),
		    'not_found' =>  __('Noticia no encontrada'),
		    'not_found_in_trash' => __('Noticia no encontrada en papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Noticias'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-megaphone',
		    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		    'taxonomies' => array('post_tag')
		);
		register_post_type('new',$args);
	}
	
	//Group Artists and Release
	add_action( 'init', 'group', 0 );
	function group() {
		$labels = array(
	    	'name' => __( 'Grupos' ),
			'singular_name' => __( 'Grupo' ),
			'search_items' =>  __( 'Buscar Grupo' ),
			'all_items' => __( 'Todos los Grupos' ),
			'parent_item' => __( 'Parent Grupo' ),
			'parent_item_colon' => __( 'Parent Grupo:' ),
			'edit_item' => __( 'Editar Grupo' ), 
			'update_item' => __( 'Actualizar Grupo' ),
			'add_new_item' => __( 'Agregar Grupo' ),
			'new_item_name' => __( 'Nombre Grupo Nuevo' ),
			'menu_name' => __( 'Grupos' ),
		); 	
	
		register_taxonomy('group',array('artist'), array(
	    	'hierarchical' => true,
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'sort' => true,
			'capability_type' => 'post',
			'rewrite' => array( 'slug' => 'group' ),
		));
	}

	//Funcion para identar JSON
	function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo indent($json);
	}
	
	//Funcion para obtener el RGBA
	function hex2rgba($color, $opacity = false) 
	{			 
		$default = 'rgb(0,0,0)';
	 
		//Return default if no color provided
		if(empty($color)) return $default; 
	 
		//Sanitize $color if "#" is provided 
		if ($color[0] == '#' ) 
		{
			$color = substr( $color, 1 );
		}
 
		//Check if color has 6 or 3 characters and get values
		if (strlen($color) == 6) 
		{
			$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} 
		elseif ( strlen( $color ) == 3 ) 
		{
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} 
		else 
		{
			return $default;
		}
 
		//Convert hexadec to rgb
		$rgb =  array_map('hexdec', $hex);
 
		//Check if opacity is set(rgba or rgb)
		if($opacity)
		{
			if(abs($opacity) > 1)
				$opacity = 1.0;
				$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
		} 
		else 
		{
			$output = 'rgb('.implode(",",$rgb).')';
		}
 
		//Return rgb(a) color string
		return $output;
	}

	//SUBIR IMAGEN A CAMPO IMG DE ACF
	function my_update_attachment($f,$pid,$t='',$c='') {
	  	wp_update_attachment_metadata( $pid, $f );
	  	if( !empty( $_FILES[$f]['name'] )) { //New upload
	    	require_once( ABSPATH . 'wp-admin/includes/file.php' );
			include( ABSPATH . 'wp-admin/includes/image.php' );
			// $override['action'] = 'editpost';
			$override['test_form'] = false;
			$file = wp_handle_upload( $_FILES[$f], $override );

			if ( isset( $file['error'] )) {
				return new WP_Error( 'upload_error', $file['error'] );
	    	}

			$file_type = wp_check_filetype($_FILES[$f]['name'], array(
				'jpg|jpeg' => 'image/jpeg',
				'gif' => 'image/gif',
				'png' => 'image/png',
			));

			if ($file_type['type']) {
				$name_parts = pathinfo( $file['file'] );
				$name = $file['filename'];
				$type = $file['type'];
				$title = $t ? $t : $name;
				$content = $c;

				$attachment = array(
					'post_title' => $title,
					'post_type' => 'attachment',
					'post_content' => $content,
					'post_parent' => $pid,
					'post_mime_type' => $type,
					'guid' => $file['url'],
				);

				foreach( get_intermediate_image_sizes() as $s ) {
					$sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
					$sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
					$sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
					$sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
	      		}

		  		$sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

		  		foreach( $sizes as $size => $size_data ) {
		  			$resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
		  			if ( $resized )
		  				$metadata['sizes'][$size] = $resized;
	      		}

		  		$attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

		  		if ( !is_wp_error( $attach_id )) {
		  			$attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
		  			wp_update_attachment_metadata( $attach_id, $attach_meta );
	      		}

		  		return array(
		  			'pid' =>$pid,
		  			'url' =>$file['url'],
		  			'file'=>$file,
		  			'attach_id'=>$attach_id
		  		);
	    	}
	  	}
	}
	

?>