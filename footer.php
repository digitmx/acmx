		<?php 
			//Leemos los Colores de Fondo
			$footer_color_start = get_field("footer_color_start", "option");
			$footer_color_end = get_field("footer_color_end", "option");
			$footer_background = '';
			
			//Construimos el Color de Fondo
			if ($footer_color_start) { $footer_background = 'background: ' . $footer_color_start . ';'; }
			if ($footer_color_start && $footer_color_end) { $footer_background = 'background: linear-gradient(to left bottom, ' . $footer_color_start . ', ' . $footer_color_end . ');'; }
		?>
		<div class="container-fluid no-margin-row" id="footer" style="position: relative;<?=($footer_background) ? ' ' . $footer_background : ''; ?>">	
			<div class="container">
				<div class="row">
					<div class="col s12 m6 l6 left roboto font16 magnesium-text inline" id="frights">
						<span class="frights-mobile"><?php echo get_field("copyright", "option"); ?></span>
					</div>
					<div class="col s12 m6 l6" id="socialbar">
						<ul class="right mobile-width-full">
							<li class="brandon font16 social inline text-center-width">
								<?php if (get_field("facebook_link","option")) { ?>
								<a href="<?php the_field("facebook_link","option"); ?>" target="_blank" class="font16 inline"><i class="fa fa-facebook magnesium-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>
								<?php } ?>
								<?php if (get_field("twitter_link","option")) { ?>
								<a href="<?php the_field("twitter_link","option"); ?>" target="_blank" class="font16 inline"><i class="fa fa-twitter magnesium-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>
								<?php } ?>
								<?php if (get_field("instagram_link","option")) { ?>
								<a href="<?php the_field("instagram_link","option"); ?>" target="_blank" class="font16 inline"><i class="fa fa-instagram magnesium-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>
								<?php } ?>
								<?php if (get_field("youtube_link","option")) { ?>
								<a href="<?php the_field("youtube_link","option"); ?>" target="_blank" class="font16 inline"><i class="fa fa-youtube-play magnesium-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>
								<?php } ?>
								<?php if (get_field("soundcloud_link","option")) { ?>
								<a href="<?php the_field("soundcloud_link","option"); ?>" target="_blank" class="font16 inline"><i class="fa fa-spotify magnesium-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>
								<?php } ?>
								<?php if (get_field("newsletter_link","option")) { ?>
								<a href="<?php the_field("newsletter_link","option"); ?>" target="_blank" class="font16 inline"><i class="fa fa-envelope magnesium-text font20 inline" id="footer-icon-social" aria-hidden="true"></i></a>
								<?php } ?>
							</li>			
						</ul>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
		<input type="hidden" id="kichink" name="kichink" value="<?php the_field("kichink_site", "option"); ?>" />
		
		<?php wp_footer(); ?>

	</body>
</html>