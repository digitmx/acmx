<?php get_header(); ?> 

	<?php create_site_menu( 'site-menu' ); ?>
	
	<!-- SINGLE NEWS -->
	<?php $image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>
	<?php $exclude_ids = array( $post->ID ); ?>
	<div class="container-fluid" id="single-news">
		<div class="row single-news">
			<div class="col s12 m8 offset-m2 l8 offset-l2 centered">
				<div class="space60"></div>
				<span class="brandon font40 uppercase"><?php the_title(); ?></span>
				<div class="space20"></div>
				<span class="helvetica light font18"><?php the_time('F j Y'); ?></span>
				<div class="space20"></div>
			</div>
			<div class="col s12 m10 offset-m1 l10 offset-l1 centered">
				<img class="responsive-img" src="<?php echo $image; ?>"/>
				<div class="space60 hide-on-small-only"></div>
			</div>
			<div class="col s12 m2 offset-m1 l2 offset-l1 right-align hide-on-small-only">
				<span class="roboto bold font18"><?php _e("COMPARTIR EN:","acmx_v1"); ?></span>
				<div class="space20"></div>
				<a href="#!" rel="<?php the_permalink($post->ID); ?>" class="roboto bold fb"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a>
				<div class="space10"></div>
				<a href="#!" title="<?php echo $post->post_title; ?>" rel="<?php the_permalink($post->ID); ?>" class="roboto bold tw"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
				<div class="space20"></div>
			</div>
			<div class="col s12 m6 offset-m1 l6 offset-l1 helvetica font14 mobile-single-news">
				<?php the_content(); ?>
				<div class="space40"></div>
				<?php $posttags = wp_get_object_terms( $post->ID,  'post_tag' ); ?>
				<?php if ($posttags) { ?>
					<div class="centered">
						<?php foreach($posttags as $tag) { ?>
						<div class="chip">
							<a class="social-color" href="<?php echo get_term_link( $tag ); ?>">
								<?php echo $tag->name; ?>
							</a>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="row show-on-small">
		<div class="col s12 centered" id="social-media-artists">
			<div class="space10"></div>
			<span class="roboto bold font18"><?php _e("COMPARTIR EN:","acmx_v1"); ?></span>
			<div class="space10"></div>
			<a href="#!" rel="<?php the_permalink($post->ID); ?>" class="roboto bold fb"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a>
			<a href="#!" title="<?php echo $post->post_title; ?>" rel="<?php the_permalink($post->ID); ?>" class="roboto bold tw"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
			<div class="space20"></div>
		</div>
	</div>
			
	<!-- MOST RECENT -->
	<?php 
	    //Query News
	    $args = array(
			'posts_per_page'   => 3,
			'order'			   => 'date',
			'orderby'          => 'DESC',
			'post_type'        => 'new',
			'post_status'      => 'publish',
			'post__not_in'	   => $exclude_ids,
			'suppress_filters' => false 
		);
		$posts_array = new WP_Query( $args ); 
	?>
	<div class="container-fluid" id="most-recent">
		<div class="row">
				<div class="col s12 m12 l12 brandon font48 centered">
					<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
					<div class="space40"></div>
				</div>
			<div class="container">
				<?php foreach ($posts_array->posts as $new) { $image = get_the_post_thumbnail_url( $new->ID, $size = 'full' ); ?>
				<div class="col s12 m4 l4">
					<a href="<?php echo get_permalink($new->ID); ?>">	
						<div class="card c-size black-text hoverable">
			            	<div class="card-image size-img">
								<img class="responsive-img" src="<?php echo $image; ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold titulo-busqueda font14"><?php echo $new->post_title; ?></p>
								<div class="space10"></div>
								<p class="roboto font12 truncate"><?php echo $new->post_excerpt; ?></p>
							</div>
			        	</div>
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- NEWSLETTER -->
		<div class="container-fluid grayer no-margin-row" id="newsletter-page">
			<div class="row">
				<div class="col s12 m12 l12 roboto bold font18 centered">
					<div class="space40"></div>
					<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
					<div class="space20"></div>
				</div>
				<div class="input-field col s12 m12 l6 offset-l3 mobile-margin60">
			        <!-- Begin MailChimp Signup Form -->
				   	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
					<style type="text/css">
						#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
					</style>
					<div id="mc_embed_signup">
						<form style="padding: 0px;" action="https://arts-crafts.us1.list-manage.com/subscribe/post?u=eda763f35af955af8a0f60dff&amp;id=94fc15a764" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						    <div id="mc_embed_signup_scroll">								
								<input style="text-indent: 10px; border: 0; height: 46px; margin-bottom: 1em; width: 100%;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php _e("Correo Electrónico","acmx_v1"); ?>" required>
							    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_eda763f35af955af8a0f60dff_94fc15a764" tabindex="-1" value=""></div>
							    <div class="clear">
								    <input style="background-color: #000; color: #fff; margin-top: -1px; border: 0; width: 100%; height: 63px; border-radius: 0px; font-family: roboto; font-size: 18px;" type="submit" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="mobile-input button">
								</div>
						    </div>
						</form>
					</div>
			        <div class="space60 hide-on-small-only"></div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>