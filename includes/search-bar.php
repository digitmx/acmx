			
			<!-- Full Screen Search -->
			<div id="search">
			    <button type="button" id="close" class="light font48">X</button>
			    <form>
			        <input type="search" value="" class="roboto light font48" placeholder="<?php _e("Buscar...","acmx_v1"); ?>" />
				</form>
				<div class="space200"></div>
				<div class="container-fluid">
					<div class="row">
					    <div class="col s12 m5 offset-m1 l5 offset-l1">
						    <div class="col s12 m12 l12">
					    		<span class="roboto bold font18 white-text"><?php _e("ARTISTAS","acmx_v1"); ?></span></br>
								<div class="space20"></div>
					    	</div>
						    <div class="col s4 m4 l4">
						    	<img class="responsive-img centered-and-cropped inline search-menu" src="<?php the_field("cat_3", "option") ?>"/>
						    </div>
						    <div class="col s6 m6 l6">
							    <ul id="listserch" class="roboto light font18 white-text inline">
								    <li><?php _e("ALABAMA","acmx_v1"); ?></li>
								    <li><?php _e("ALGODON","acmx_v1"); ?></li>
								    <li><?php _e("AMY MILLAN","acmx_v1"); ?></li>
								    <li><?php _e("ANIMAL COLLECTIVE","acmx_v1"); ?></li>
								    <li><?php _e("ALABAMA","acmx_v1"); ?></li>
								    <li><?php _e("ALGODON","acmx_v1"); ?></li>
								    <li><?php _e("AMY MILLAN","acmx_v1"); ?></li>
								    <li><?php _e("ANIMAL COLLECTIVE","acmx_v1"); ?></li>
								    <li class="underline"><a href="#!" class="white-text"><?php _e("VER TODOS","acmx_v1"); ?></a></li>
							    </ul>
						    </div>
					    </div>
					    <div class="col s12 m5 l5">
						    <div class="col s12 m12 l12">
					    		<span class="roboto bold font18 white-text"><?php _e("CATALOGO","acmx_v1"); ?></span></br>
					    		<div class="space20"></div>
					    	</div>
						    <div class="col s4 m4 l4">
						    	<img class="responsive-img centered-and-cropped inline search-menu" src="<?php the_field("tour_artists", "option") ?>"/>
						    </div>
						    <div class="col s6 m6 l6">
							    <ul id="listserch" class="roboto light font18 white-text">
								    <li><?php _e("ALABAMA","acmx_v1"); ?></li>
								    <li><?php _e("ALGODON","acmx_v1"); ?></li>
								    <li><?php _e("AMY MILLAN","acmx_v1"); ?></li>
								    <li><?php _e("ANIMAL COLLECTIVE","acmx_v1"); ?></li>
								    <li class="underline"><a href="#!" class="white-text"><?php _e("VER TODOS","acmx_v1"); ?></a></li>
							    </ul>
						    </div>
					    </div>
					</div>
					<div class="row">
						<div class="col s12 m10 offset-m1 l10 offset-l1">
							<span class="roboto bold font18 white-text"><?php _e("NOTICIAS","acmx_v1"); ?></span>
							<a class="right roboto light font18 underline white-text" href="#!"><?php _e("VER TODOS","acmx_v1"); ?></a>
							<div class="space20"></div>
						</div>
						<div class="col s12 m5offset-m1 l5 offset-l1">
							<div class="col s4 m4 l4">
							    <img class="responsive-img centered-and-cropped search-menu" src="<?php the_field("pro_7", "option") ?>"/>
								<div class="space20"></div>
						    </div>
						    <div class="col s8 m8 l8">
							    <div class="space40"></div>
							    <span class="roboto bold font18 block white-text">JAPANDROIDS ESTRENA NUEVO SENCILLO DE CARA AL ESTRENO DE SU DISCO</span>
							    <div class="space20"></div>
							    <span class="helvetica light font16 magnesium-text block">MARCH 13 2017</span>
						    </div>
						</div>
						<div class="col s12 m5 l5">
							<div class="col s4 m4 l4">
							    <img class="responsive-img centered-and-cropped search-menu" src="<?php the_field("pro_7", "option") ?>"/>
								<div class="space20"></div>
						    </div>
						    <div class="col s8 m8 l8">
							    <div class="space40"></div>
							    <span class="roboto bold font18 block white-text">JAPANDROIDS ESTRENA NUEVO SENCILLO DE CARA AL ESTRENO DE SU DISCO</span>
							    <div class="space20"></div>
							    <span class="helvetica light font16 magnesium-text block">MARCH 13 2017</span>
						    </div>
						</div>
						<div class="col s12 m5 offset-m1 l5 offset-l1">
							<div class="col s4 m4 l4">
							    <img class="responsive-img centered-and-cropped search-menu" src="<?php the_field("pro_7", "option") ?>"/>
								<div class="space20"></div>
						    </div>
						    <div class="col s8 m8 l8">
							    <div class="space40"></div>
							    <span class="roboto bold font18 block white-text">JAPANDROIDS ESTRENA NUEVO SENCILLO DE CARA AL ESTRENO DE SU DISCO</span>
							    <div class="space20"></div>
							    <span class="helvetica light font16 magnesium-text block">MARCH 13 2017</span>
						    </div>
						</div>
						<div class="col s12 m5 l5">
							<div class="col s4 m4 l4">
							    <img class="responsive-img centered-and-cropped search-menu" src="<?php the_field("pro_7", "option") ?>"/>
								<div class="space20"></div>
						    </div>
						    <div class="col s8 m8 l8">
							    <div class="space40"></div>
							    <span class="roboto bold font18 block white-text">JAPANDROIDS ESTRENA NUEVO SENCILLO DE CARA AL ESTRENO DE SU DISCO</span>
							    <div class="space20"></div>
							    <span class="helvetica light font16 magnesium-text block">MARCH 13 2017</span>
						    </div>
						</div>
					</div>
				</div>
			</div>