
 $(document).ready(function(){
	 
	//Read Values 
	var base_url = $('#base_url').val();
	var kichink = $('#kichink').val();
	console.log(kichink);
	
	//FB Setup
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
	    FB.init({
	    	appId: '316337082185265',
			version: 'v2.11' // or v2.1, v2.2, v2.3, ...
	    });
	});
	
	$('.carousel.carousel-slider').carousel({fullWidth: true});
	
	//Kichink Config
	$('#kichink-shoppingkart').ShoppingKart({
	    text: 'ARTS & CRAFTS',
	    beforeText: 'COMPRAR',
		store_id: kichink,
		theme: 'button',
		button: '#kichink-buybutton',
		placement: 'left',
		showOnPurchase: true,
		checkoutURI: 'https://www.kichink.com/checkout'
	});
	 
	//Button Mobile
	$(".button-collapse").sideNav();
	
	//Material Box
	$('.materialboxed').materialbox();

 	//Slider Site
    $('.slider').slider({ 
		  	indicators: true,
		  	height: 685
      });
      
	//Catalog Carousel      
    $('.responsive').slick({
	  		speed: 300,
	  		slidesToShow: 13,
	  		slidesToScroll: 13,
	  		responsive: [
	  			{
	  				breakpoint: 1024,
	  				settings: {
	  				slidesToShow: 13,
	  				slidesToScroll: 13,
      				}
    			},
				{
					breakpoint: 600,
					settings: {
					slidesToShow: 7,
					slidesToScroll: 7
      				}
    			},
				{
					breakpoint: 480,
					settings: {
					slidesToShow: 7,
					slidesToScroll: 7
      				}
      			}
      		]
	  	});

	//Artists Carousel
	$('.carousel').carousel({ });
    
	$("#menu-des1").click(function(){
		$("#menu1").toggle();
	});
		    
	$('#btnSearch').on('click', function(event) {
    	event.preventDefault();
    	$('.navbar-fixed').css("z-index" , "1003");
		$('#search').addClass('open');
		$('#search > form > input[type="search"]').focus();
    });
	    
    /*$('#search, #close').on('click keyup', function(event) {
	    $(this).removeClass('open');
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });*/
	    
    /*$('form').submit(function(event) {
        event.preventDefault();
        return false;
    });*/
	    
	$('#catalog #catalog-artists').on('click','.section span', function(e) {
	    e.preventDefault();
	    
	    //Read Rel
	    var section = $(this).attr('rel');
	    
	    //Remove Class Active
	    $('#catalog #catalog-artists .section span').removeClass('active');
	    $(this).addClass('active');
	    $('#catalog #catalog-table #active_section').val(section);
	    
	    //Check Section
	    if (section == 'artists') 
	    {
		    $('#catalog #catalog-table .container_catalog').fadeOut(0);
		    $('#catalog #catalog-table .container_artists').fadeIn('slow');
		    $('#catalog #catalog-table .filters_letters').fadeIn('slow');
	    }
	    else
	    {
		    $('#catalog #catalog-table .container_artists').fadeOut(0);
		    $('#catalog #catalog-table .container_catalog').fadeIn('slow');
		    $('#catalog #catalog-table .filters_letters').fadeOut(0);
	    }
	    
	    return false;
    });
	
	//Button Backward Artists
    $('#catalog #catalog-table .container_artists').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    var filter = $('#catalog #catalog-table #filter_artists').val();
	    var page = $('#catalog #catalog-table #artists_page').val();
	    var max_page = $('#catalog #catalog-table #artists_max_page').val();
	    var error_artists_max_page = $('#catalog #catalog-table #error_artists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var total_prev_page = 6 * prevpage;
	    var param = '{"msg": "getArtists","fields": {"filter": "' + filter + '","page": "' + prevpage + '","lang": "' + lang + '"}}';
	    console.log(param);
	    
		if (filter != 'all')
		{
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#catalog-table .container_artists .items_artists').html(data.html);
					$(document).scrollTop( $('.text-center-container').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
		}
		else
		{
			//Hide Containers Artists
			$('#catalog #catalog-table .items_artists').fadeOut(0);
			
			//Show Next Artists
			$('#catalog #catalog-table #artists_' + prevpage).fadeIn('slow');
		}
		
		//Update Page Artists
		$('#catalog #catalog-table #artists_page').val(prevpage);
		
		//Check Forward 
		if (prevpage == 1)
		{
			//Hide Button
		    $('#catalog #catalog-table .container_artists .bt-bk').css('display','none');
		}
		
		//Show Button
		$('#catalog #catalog-table .container_artists .bt-fr').css('display','inline-block');
		
		$(document).scrollTop( $('#catalog').offset().top );
	    
	    return false;
    });
    
    //Button Forward Artists
    $('#catalog #catalog-table .container_artists').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    var filter = $('#catalog #catalog-table #filter_artists').val();
	    var page = $('#catalog #catalog-table #artists_page').val();
	    var max_page = $('#catalog #catalog-table #artists_max_page').val();
	    var error_artists_max_page = $('#catalog #catalog-table #error_artists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var total_next_page = 6 * nextpage;
	    var param = '{"msg": "getArtists","fields": {"filter": "' + filter + '","page": "' + nextpage + '","lang": "' + lang + '"}}';
	    console.log(param);
	    
	    if (filter != 'all')
		{
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#catalog-table .container_artists .items_artists').html(data.html);
					$(document).scrollTop( $('.text-center-container').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
		}
		else
		{
			//Hide Containers Playlists
			$('#catalog #catalog-table .items_artists').fadeOut(0);
			
			//Show Next Playlist
			$('#catalog #catalog-table #artists_' + nextpage).fadeIn('slow');
		}
		
		//Update Page Artists
		$('#catalog #catalog-table #artists_page').val(nextpage);
		
		//Check Forward 
		if (max_page <= total_next_page)
		{
			//Hide Button
		    $('#catalog #catalog-table .container_artists .bt-fr').css('display','none');
		}
		
		//Show Button
		$('#catalog #catalog-table .container_artists .bt-bk').css('display','inline-block');
		
		$(document).scrollTop( $('#catalog').offset().top );
	    
	    return false;
    });
    
	/*    
    $('#catalog #catalog-table .container_artists').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    var filter = $('#catalog #catalog-table #filter_artists').val();
	    var page = $('#catalog #catalog-table #artists_page').val();
	    var max_page = $('#catalog #catalog-table #artists_max_page').val();
	    var error_artists_max_page = $('#catalog #catalog-table #error_artists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var param = '{"msg": "getArtists","fields": {"filter": "' + filter + '","page": "' + prevpage + '","lang": "' + lang + '"}}';
	    
		//Check Next Page
		if (prevpage > 0)
		{
			//Show Button
			if (prevpage == 1)
			{
				//Show Button
				$('#catalog #catalog-table .container_artists .bt-bk').css('display','none');
			}
			
			if (prevpage != max_page)
			{
				$('#catalog #catalog-table .container_artists .bt-fr').css('display','inline-block');
			}
			
			//Show Loading
			$('#catalog-table .container_artists .items_artists').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
			
			//Update Page Artists
			$('#catalog #catalog-table #artists_page').val(prevpage);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#catalog-table .container_artists .items_artists').html(data.html);
					$(document).scrollTop( $('.text-center-container').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
	    }
	    else
	    {
		    //Hide Button
		    $('#catalog #catalog-table .container_artists .bt-bk').css('display','none');
		    
		    //Show Error
			Materialize.toast(error_artists_max_page, 4000);
	    }
	    
	    return false;
    });
	    
    $('#catalog #catalog-table .container_artists').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    var filter = $('#catalog #catalog-table #filter_artists').val();
	    var page = $('#catalog #catalog-table #artists_page').val();
	    var max_page = $('#catalog #catalog-table #artists_max_page').val();
	    var error_artists_max_page = $('#catalog #catalog-table #error_artists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var param = '{"msg": "getArtists","fields": {"filter": "' + filter + '","page": "' + nextpage + '","lang": "' + lang + '"}}';
	    
		//Check Next Page
		if (nextpage <= max_page)
		{
			//Show Button
			$('#catalog #catalog-table .container_artists .bt-bk').css('display','inline-block');
			
			//Hide Button
			if (nextpage == max_page)
			{
				$('#catalog #catalog-table .container_artists .bt-fr').css('display','none');
			}
			else
			{
				$('#catalog #catalog-table .container_artists .bt-fr').css('display','inline-block');
			}
			
			//Show Loading
			$('#catalog-table .container_artists .items_artists').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
			
			//Update Page Artists
			$('#catalog #catalog-table #artists_page').val(nextpage);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#catalog-table .container_artists .items_artists').html(data.html);
					$(document).scrollTop( $('.text-center-container').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
	    }
	    else
	    {
		    //Hide Button
		    $(this).fadeOut(0);
		    
		    //Show Error
			Materialize.toast(error_artists_max_page, 4000);
	    }
	    
	    return false;
    });
    */
        
    $('#catalog #catalog-table .container_catalog').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    var filter = $('#catalog #catalog-table #filter_catalog').val();
	    var page = $('#catalog #catalog-table #catalog_page').val();
	    var max_page = $('#catalog #catalog-table #catalog_max_page').val();
	    var error_catalog_max_page = $('#catalog #catalog-table #error_catalog_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var param = '{"msg": "getReleases","fields": {"filter": "' + filter + '","page": "' + prevpage + '","lang": "' + lang + '"}}';
	    
		//Check Next Page
		if (prevpage > 0)
		{
			//Show Button
			if (prevpage == 1)
			{
				//Show Button
				$('#catalog #catalog-table .container_catalog .bt-bk').css('display','none');
			}
			
			if (prevpage != max_page)
			{
				$('#catalog #catalog-table .container_catalog .bt-fr').css('display','inline-block');
			}
			
			//Show Loading
			$('#catalog-table .container_catalog .items_releases').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
			
			//Update Page Artists
			$('#catalog #catalog-table #catalog_page').val(prevpage);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#catalog-table .container_catalog .items_releases').html(data.html);
					$(document).scrollTop( $('#catalog').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
	    }
	    else
	    {
		    //Hide Button
		    $('#catalog #catalog-table .container_catalog .bt-bk').css('display','none');
		    
		    //Show Error
			Materialize.toast(error_artists_max_page, 4000);
	    }
	    
	    return false;
    });
	    
    $('#catalog #catalog-table .container_catalog').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    var filter = $('#catalog #catalog-table #filter_catalog').val();
	    var page = $('#catalog #catalog-table #catalog_page').val();
	    var max_page = $('#catalog #catalog-table #catalog_max_page').val();
	    var error_artists_max_page = $('#catalog #catalog-table #error_catalog_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var param = '{"msg": "getReleases","fields": {"filter": "' + filter + '","page": "' + nextpage + '","lang": "' + lang + '"}}';
	    
		//Check Next Page
		if (nextpage <= max_page)
		{
			//Show Button
			$('#catalog #catalog-table .container_catalog .bt-bk').css('display','inline-block');
			
			//Hide Button
			if (nextpage == max_page)
			{
				$('#catalog #catalog-table .container_catalog .bt-fr').css('display','none');
			}
			else
			{
				$('#catalog #catalog-table .container_catalog .bt-fr').css('display','inline-block');
			}
			
			//Show Loading
			$('#catalog-table .container_catalog .items_releases').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
			
			//Update Page Artists
			$('#catalog #catalog-table #catalog_page').val(nextpage);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#catalog-table .container_catalog .items_releases').html(data.html);
					$(document).scrollTop( $('#catalog').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
	    }
	    else
	    {
		    //Hide Button
		    $(this).fadeOut(0);
		    
		    //Show Error
			Materialize.toast(error_artists_max_page, 4000);
	    }
	    
	    return false;
    });
    
    $('#catalog #catalog-table .filter_letter').on('click', function(e) {
	    e.preventDefault();
	    
	    //Clear Bold Letters
	    $('#catalog #catalog-table .filter_letter').css('font-weight','normal');
	    $('#catalog #catalog-table .slick-list.draggable a').css('color','#a2a2a2');
	    $(this).css('font-weight','bold');
	    $(this).css('color','#FFF');
	    
	    //Read Values
	    var letter = $(this).attr('filter');
	    var lang = $('#catalog #catalog-table #lang_site').val();
	    $('#catalog #catalog-table #filter_artists').val(letter);
		$('#catalog #catalog-table #artists_page').val('1');
	    var error_artists_max_page = $('#catalog #catalog-table #error_artists_max_page').val();
	    var param = '{"msg": "getArtists","fields": {"filter": "' + letter + '","page": "1","lang": "' + lang + '"}}';
	    
	    //Show Loading
		$('#catalog-table .container_artists .items_artists').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
	    
	    //Request API
		$.post(base_url + '/api', { param: param }).done(function( data ) {
			console.log(data);
			//Check Result
			if (data.status == 1)
			{
				$('#catalog #catalog-table #artists_max_page').val(data.max_pages);
				var max_page = data.max_pages;
			    var nextpage = parseInt(1) + 1;
			    var prevpage = 1;
			    
			    //Show Button
				if (prevpage == 1)
				{
					//Show Button
					$('#catalog #catalog-table .container_artists .bt-bk').css('display','none');
				}
				
				if (prevpage != max_page)
				{
					$('#catalog #catalog-table .container_artists .bt-fr').css('display','inline-block');
				}
				
				if (max_page == 1)
				{
					$('#catalog #catalog-table .container_artists .bt-fr').css('display','none');
				}
				
				//Update Page Artists
				$('#catalog #catalog-table #artists_page').val(prevpage);
				
				//Show HTML Response
				$('#catalog-table .container_artists .items_artists').html(data.html);
			}
			else
			{
				//Hide Button
			    $('#catalog #catalog-table .container_artists .bt-bk').css('display','none');
			    
			    //Show Error
				Materialize.toast(error_artists_max_page, 4000);
			}
		});
	    
	    return false;
    });
    
    //Button Forward Playlist
    $('#playlist #play-content .container_playlists').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#playlist #play-content #lang_site').val();
	    var page = $('#playlist #play-content #playlists_page').val();
	    var max_page = $('#playlist #play-content #playlists_max_page').val();
	    var error_playlists_max_page = $('#playlist #play-content #error_playlists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var total_next_page = 6 * nextpage;
	    //var param = '{"msg": "getArtists","fields": {"filter": "' + filter + '","page": "' + nextpage + '","lang": "' + lang + '"}}';
	    
		//Hide Containers Playlists
		$('#playlist #play-content .items_playlists').fadeOut(0);
		
		//Show Next Playlist
		$('#playlist #play-content #playlists_' + nextpage).fadeIn('slow');
		
		//Update Page Artists
		$('#playlist #play-content #playlists_page').val(nextpage);
		
		//Check Forward 
		if (max_page < total_next_page)
		{
			//Hide Button
		    $('#playlist #play-content .container_playlists .bt-fr').css('display','none');
		}
		
		//Show Button
		$('#playlist #play-content .container_playlists .bt-bk').css('display','block');
		
		$(document).scrollTop( $('#playlist').offset().top );
	    
	    return false;
    });
    
    //Button Backward Playlist
    $('#playlist #play-content .container_playlists').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#playlist #play-content #lang_site').val();
	    var page = $('#playlist #play-content #playlists_page').val();
	    var max_page = $('#playlist #play-content #playlists_max_page').val();
	    var error_playlists_max_page = $('#playlist #play-content #error_playlists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var total_prev_page = 6 * prevpage;
	    //var param = '{"msg": "getArtists","fields": {"filter": "' + filter + '","page": "' + nextpage + '","lang": "' + lang + '"}}';
	    
		//Hide Containers Playlists
		$('#playlist #play-content .items_playlists').fadeOut(0);
		
		//Show Next Playlist
		$('#playlist #play-content #playlists_' + prevpage).fadeIn('slow');
		
		//Update Page Artists
		$('#playlist #play-content #playlists_page').val(prevpage);
		
		//Check Forward 
		if (prevpage == 1)
		{
			//Hide Button
		    $('#playlist #play-content .container_playlists .bt-bk').css('display','none');
		}
		
		//Show Button
		$('#playlist #play-content .container_playlists .bt-fr').css('display','block');
		
		$(document).scrollTop( $('#playlist').offset().top );
	    
	    return false;
    });
    
    //Hover Event
    $('#calendar .event').hover(function(e) {
	    e.preventDefault();
	    
	    var rel = $(this).attr('rel');
	    $('#calendar #artist-tour .poster').attr('src', rel);
	    
	    return false;
    });
    
    //Click Buy Tickets
    $('.event #btbuy-e').on('click', function(e) {
	    e.preventDefault();
	    
	    var url = $(this).attr('rel');
	    
	    window.open(url);   
	    
	    return false;
    });
    
    //Click Videos
    $('.video').on('click', function(e) {
	    e.preventDefault();
	    
	    var youtube_id = $(this).attr('youtube_id');
	    var title = $(this).attr('title');
	    var artist = $(this).attr('artist');
	    var description = $(this).attr('description');
	    
	    $('#vid-info #video_artist').text(artist);
	    $('#vid-info #video_title').text(title);
	    $('#vid-info #video_description').text(description);
	    $('#vid-show').html('<iframe src="https://www.youtube.com/embed/' + youtube_id + '" frameborder="0" allowfullscreen></iframe>');
	    
	    return false;
    });
    
    //Click Back Videos Page
    $('#videos').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    var current = $('#current_page').val();
	    var pages = $('#pages').val();
	    var prev = parseInt(current) - 1;
	    	    
	    if (prev < pages)
	    {
		    //Show Next Page
		    $('#video_page_' + current).fadeOut(0);
		    $('#video_page_' + prev).fadeIn('slow');
		    
		    //Check if last page
		    if (prev.toString() == '1') { $(this).fadeOut(0); }
		    
		    //Show Prev Button Page Video
		    $('#videos .bt-fr').fadeIn('slow');
		    
		    //Update Page
		    $('#current_page').val(prev);
	    }
	    
	    return false;
    });
    
    //Click Forward Videos Page
    $('#videos').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    var current = $('#current_page').val();
	    var pages = $('#pages').val();
	    var next = parseInt(current) + 1;
	    
	    if (pages > current)
	    {
		    //Show Next Page
		    $('#video_page_' + current).fadeOut(0);
		    $('#video_page_' + next).fadeIn('slow');
		    
		    //Check if last page
		    if (next == pages) { $(this).fadeOut(0); }
		    
		    //Show Prev Button Page Video
		    $('#videos .bt-bk').fadeIn('slow');
		    
		    //Update Page
		    $('#current_page').val(next);
	    }
	    
	    return false;
    });
    
    //fb Click
    $('.fb').on('click', function(e) {
	    e.preventDefault();
	    var url = $(this).attr('rel');
		
		FB.ui({
			method: 'share',
			href: url,
			hashtag: '',
			mobile_iframe: true,
		}, function(response){});

        return false;
    });
    
    //tw Click
	$('.tw').on('click', function(e) {
		e.preventDefault();
		var url = $(this).attr('rel');
		var title = $(this).attr('title');
		
		var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = 'http://twitter.com/share?url=' + url + '&text=' + title,
            opts   = 'status=1' +
                     ',width='  + width  +
                     ',height=' + height +
                     ',top='    + top    +
                     ',left='   + left;

        window.open(url, 'twitter', opts);

        return false;
    });
    
    //pi Click
	$('.pi').on('click', function(e) {
		e.preventDefault();
		var url = $(this).attr('rel');
		var title = $(this).attr('title');
		
		var width  = 575,
            height = 400,
            left   = ($(window).width()  - width)  / 2,
            top    = ($(window).height() - height) / 2,
            url    = 'http://pinterest.com/pin/create/button/?url=' + url + '&description=' + title,
            opts   = 'status=1' +
                     ',width='  + width  +
                     ',height=' + height +
                     ',top='    + top    +
                     ',left='   + left;

        window.open(url, 'pinterest', opts);

        return false;
    });
    
    $('#all-art-cat #catalogo').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#all-art-cat #catalogo #lang_site').val();
	    var filter = $('#all-art-cat #catalogo #filter_catalog').val();
	    var page = $('#all-art-cat #catalogo #catalog_page').val();
	    var max_page = $('#all-art-cat #catalogo #catalog_max_page').val();
	    var error_catalog_max_page = $('#all-art-cat #catalogo #error_catalog_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var param = '{"msg": "getReleasesCatalog","fields": {"filter": "' + filter + '","page": "' + prevpage + '","lang": "' + lang + '"}}';
	    
		//Check Next Page
		if (prevpage > 0)
		{
			//Show Button
			if (prevpage == 1)
			{
				//Show Button
				$('#all-art-cat #catalogo .bt-bk').css('display','none');
			}
			
			if (prevpage != max_page)
			{
				$('#all-art-cat #catalogo .bt-fr').css('display','inline-block');
			}
			
			//Show Loading
			$('#all-art-cat #catalogo .results_catalog').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
			
			//Update Page Artists
			$('#all-art-cat #catalogo #catalog_page').val(prevpage);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#all-art-cat #catalogo .results_catalog').html(data.html);
					$(document).scrollTop( $('#all-art-cat #catalogo').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
	    }
	    else
	    {
		    //Hide Button
		    $('#all-art-cat #catalogo .bt-bk').css('display','none');
		    
		    //Show Error
			Materialize.toast(error_artists_max_page, 4000);
	    }
	    
	    return false;
    });
	    
    $('#all-art-cat #catalogo').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#all-art-cat #catalogo #lang_site').val();
	    var filter = $('#all-art-cat #catalogo #filter_catalog').val();
	    var page = $('#all-art-cat #catalogo #catalog_page').val();
	    var max_page = $('#all-art-cat #catalogo #catalog_max_page').val();
	    var error_artists_max_page = $('#all-art-cat #catalogo #error_catalog_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var param = '{"msg": "getReleasesCatalog","fields": {"filter": "' + filter + '","page": "' + nextpage + '","lang": "' + lang + '"}}';
	    
		//Check Next Page
		if (nextpage <= max_page)
		{
			//Show Button
			$('#all-art-cat #catalogo .bt-bk').css('display','inline-block');
			
			//Hide Button
			if (nextpage == max_page)
			{
				$('#all-art-cat #catalogo .bt-fr').css('display','none');
			}
			else
			{
				$('#all-art-cat #catalogo .bt-fr').css('display','inline-block');
			}
			
			//Show Loading
			$('#all-art-cat #catalogo .results_catalog').html('<center><div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></center>');
			
			//Update Page Artists
			$('#all-art-cat #catalogo #catalog_page').val(nextpage);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
	
				//Check Result
				if (data.status == 1)
				{
					//Show HTML Response
					$('#all-art-cat #catalogo .results_catalog').html(data.html);
					$(document).scrollTop( $('#all-art-cat #catalogo').offset().top );
				}
				else
				{
					//Show Error
					Materialize.toast(data.msg, 4000);
				}
				
			});
	    }
	    else
	    {
		    //Hide Button
		    $(this).fadeOut(0);
		    
		    //Show Error
			Materialize.toast(error_artists_max_page, 4000);
	    }
	    
	    return false;
    });
    
    //Button Forward Playlist
    $('#all-art-cat #playlists').on('click', '.bt-fr', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#all-art-cat #playlists #lang_site').val();
	    var page = $('#all-art-cat #playlists #playlists_page').val();
	    var max_page = $('#all-art-cat #playlists #playlists_max_page').val();
	    var error_playlists_max_page = $('#all-art-cat #playlists #error_playlists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var total_next_page = 6 * nextpage;
	    console.log(nextpage);
	    console.log(max_page);
	    
		//Hide Containers Playlists
		$('#all-art-cat #playlists .items_playlists').fadeOut(0);
		
		//Show Next Playlist
		$('#all-art-cat #playlists #playlists_' + nextpage).fadeIn('slow');
		
		//Update Page Artists
		$('#all-art-cat #playlists #playlists_page').val(nextpage);
		
		//Check Forward 
		if (max_page < total_next_page)
		{
			//Hide Button
		    $('#all-art-cat #playlists .bt-fr').css('display','none');
		}
		
		//Show Button
		$('#all-art-cat #playlists .bt-bk').css('display','block');
		
		$(document).scrollTop( $('#all-art-cat #playlists').offset().top );
	    
	    return false;
    });
    
    //Button Backward Playlist
    $('#all-art-cat #playlists').on('click', '.bt-bk', function(e) {
	    e.preventDefault();
	    
	    //Read Data
	    var lang = $('#all-art-cat #playlists #lang_site').val();
	    var page = $('#all-art-cat #playlists #playlists_page').val();
	    var max_page = $('#all-art-cat #playlists #playlists_max_page').val();
	    var error_playlists_max_page = $('#all-art-cat #playlists #error_playlists_max_page').val();
	    var nextpage = parseInt(page) + 1;
	    var prevpage = parseInt(page) - 1;
	    var total_prev_page = 6 * prevpage;
	    
		//Hide Containers Playlists
		$('#all-art-cat #playlists .items_playlists').fadeOut(0);
		
		//Show Next Playlist
		$('#all-art-cat #playlists #playlists_' + prevpage).fadeIn('slow');
		
		//Update Page Artists
		$('#all-art-cat #playlists #playlists_page').val(prevpage);
		
		//Check Forward 
		if (prevpage == 1)
		{
			//Hide Button
		    $('#all-art-cat #playlists .bt-bk').css('display','none');
		}
		
		//Show Button
		$('#all-art-cat #playlists .bt-fr').css('display','block');
		
		$(document).scrollTop( $('#all-art-cat #playlists').offset().top );
	    
	    return false;
    });
    
    //mobile-menu search input
    $('#mobile-menu input').keyup(function(e) {
	    e.preventDefault();
	    
	    //Leemos el valor de búsqueda
	    var search = $.trim($('#mobile-menu #icon_prefix').val());
	    var error_search = $.trim($('#mobile-menu #error_search').val());
	    
	    //Revisamos el enter
	    if(e.which == 13) 
	    {
		    //Verificamos el término
		    if (search)
		    {
			    //Redirect a Búsqueda
			    window.location.href = base_url + '/?s=' + search;
		    }
		    else
		    {
			    //Mostramos el error del buscador
			    Materialize.toast(error_search, 4000);
		    }
		}
	    
	    return false;
    });
	
	//Modal
	$('#search-modal').modal({
		dismissible: false
	});
	
	$(window).resize(function(){
        if ($(window).width() < 992) {
			$('.modal-close').trigger("click");
		}
    });
    
    $('#search_input').keypress(function(e) {
	    if(e.which == 13) 
	    {
		    e.preventDefault();
	    
	        var keyword = $(this).val();
			
			window.location.href = base_url + '/?s=' + keyword;
			
			return false;
	    }
	});
	
	//search function
	function searchDesktop(keyword) 
	{
		//API Call
		var param = '{"msg": "search","fields": {"keyword": "' + keyword + '"}}';
		
		//Request API
		$.post(base_url + '/api', { param: param }).done(function( data ) {

			//Check Result
			if (data.status == 1)
			{
				//Show HTML Response
				$('.results_artists').html(data.data.html_artists);
				$('.results_releases').html(data.data.html_releases);	
				$('.results_news').html(data.data.html_news);			
			}
			else
			{
				//Show Error
				Materialize.toast(data.msg, 4000);
			}
			
		});
	}
	
	//search_input type
	$('#search_input').keyup(function() {
		var keyword = $(this).val();
		if (keyword.length > 1)
		{
			//Do Search
			searchDesktop(keyword);
		}
		else
		{
			//Hide Results
			$('.results_artists').html('');
			$('.results_releases').html('');
			$('.results_news').html('');
		}
	});
	
	//Center Images News
	$( ".aligncenter" ).parent().addClass( "centered" );
	
	//Responsive Img Fix
	if ($( ".mobile-single-news p img" ).length) {
		$(".mobile-single-news p img").addClass("responsive-img");
	}
	
	//iframe Fix
	$( "iframe" ).parent().addClass( "centered" );
	
});        