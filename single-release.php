<?php get_header(); ?> 

	<?php create_site_menu( 'site-menu' ); ?>
		
	<!-- Artists Bio -->
	<?php 
		$image = get_the_post_thumbnail_url( $post->ID, $size = 'full' ); 
	?>
	<div class="container-fluid">
		<div id="artists-bio">
			<div class="space20"></div>
			<?php $release = $post; ?>
			<div class="row">
				<div class="col s12 m6 offset-m3 l6 offset-l3">
					<img class="responsive-img" src="<?php echo $image; ?>" />					
				</div>
				<div class="col s12 m6 offset-m3 l6 offset-l3 hide-on-small-only" id="social-media-artists">
					<div class="centered">
						<div class="space20"></div>
						<span class="roboto bold font18"><?php _e("COMPARTIR EN:","acmx_v1"); ?></span>
						<div class="space10"></div>
						<a href="#!" rel="<?php the_permalink($post->ID); ?>" class="roboto bold fb"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a>
						&nbsp;&nbsp;<a href="#!" title="<?php echo $post->post_title; ?>" rel="<?php the_permalink($post->ID); ?>" class="roboto bold tw"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
						<div class="space20"></div>
						<?php if (get_field("release_digital",$release->ID)) { ?>
						<span class="roboto bold font18"><?php _e("LO QUIERO EN:","acmx_v1"); ?></span>
						<div class="space10"></div>
						<span class="roboto font14 bold buy-link">
							<?php if (get_field("release_cd",$release->ID)) { ?>
							<a href="<?=(get_field("release_cd",$release->ID)) ? get_field("release_cd",$release->ID) : '#!';?>" target="<?=(get_field("release_cd",$release->ID)) ? '_blank' : ''; ?>" class="<?=(get_field("release_cd",$release->ID)) ? 'active ' : ''; ?>magnesium-text"><?php _e("CD","acmx_v1"); ?></a>
							<?php } ?>
							<?php if (get_field("release_vinyl",$release->ID)) { ?>
							<a href="<?=(get_field("release_vinyl",$release->ID)) ? get_field("release_vinyl",$release->ID) : '#!';?>" target="<?=(get_field("release_vinyl",$release->ID)) ? '_blank' : ''; ?>" class="<?=(get_field("release_vinyl",$release->ID)) ? 'active ' : ''; ?>magnesium-text"><?php _e("VINIL","acmx_v1"); ?></a>
							<?php } ?>
							<?php if (get_field("release_digital",$release->ID)) { ?>
							<a href="<?=(get_field("release_digital",$release->ID)) ? get_field("release_digital",$release->ID) : '#!';?>" target="<?=(get_field("release_digital",$release->ID)) ? '_blank' : ''; ?>" class="<?=(get_field("release_digital",$release->ID)) ? 'active ' : ''; ?>magnesium-text"><?php _e("DIGITAL","acmx_v1"); ?></a>
							<?php } ?>
						</span>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m8 offset-m2 l6 offset-l3 centered">
					<span class="brandon bold font48 uppercase"><?php echo $release->post_title; ?></span>
					<div class="space10"></div>
					<?php $release_date = get_field("release_date",$release->ID); ?>
					<p class="helvetica font14"><?php echo substr($release_date, 6, 2); ?>.<?php echo substr($release_date, 4, 2); ?>.<?php echo substr($release_date, 0, 4); ?> <?=(get_field("release_acmx",$release->ID)) ? '|' : ''; ?> <?php the_field("release_acmx",$release->ID); ?></p>
					<div class="space20"></div>
					<div class="helvetica font14 mobile-justify"><?php echo $release->post_content; ?></div>
				</div>
			</div>
			<div class="space40 hide-on-small-only"></div>
			<div class="container-fluid playlist-album">
				<div class="row">
					<div class="col s12 m12 l6 offset-l3 roboto">
						<span class="font16 magnesium-text albumn-tracks" id="album-pos" ><?php _e("ALBUM","acmx_v1"); ?></span>
						<table class="bordered" id="table-playlist">
					        <thead  class="bold">
					        	<tr>
					            	<th class="uppercase"><?php echo $release->post_title; ?></th>
					        	</tr>
					        </thead>
					
					        <tbody>
						        <?php $release_tracks = get_field("release_tracks", $release->ID); $contador_tracks = 0; ?>
						        <?php foreach ($release_tracks as $track) { $contador_tracks++; ?>
					        	<tr>
					            	<td class="uppercase"><?php echo $contador_tracks; ?> <?php echo $track['title']; ?></td>
					        	</tr>
					        	<?php } ?>
					        	</tr>
					        </tbody>
					    </table>
					</div>
					<?php if (get_field("release_apple_music",$release->ID) || get_field("release_deezer",$release->ID) || get_field("release_google_play",$release->ID) || get_field("release_spotify",$release->ID)) { ?>
					<div class="col s12 m12 l8 offset-l2 roboto bold">
						<div class="space20 hide-on-med-and-down"></div>
						<div class="mobile-artist-center centered">
							<span><?php _e("TAMBIÉN ESCUCHANOS EN:","acmx_v1"); ?></span>
							<div class="space20"></div>
							<?php if (get_field("link_spotify",$release->ID)) { ?>
							<a href="<?php the_field("link_spotify",$release->ID); ?>" target="_blank"><img id="music-deezer" src="<?php bloginfo("template_directory"); ?>/img/spotify.png"/></a>
							<?php } ?>
							<?php if (get_field("release_apple_music",$release->ID)) { ?>
							<a href="<?php the_field("release_apple_music",$release->ID); ?>" target="_blank"><img id="music-deezer" src="<?php bloginfo("template_directory"); ?>/img/music.svg"/></a>
							<?php } ?>
							<?php if (get_field("release_deezer",$release->ID)) { ?>
							<a href="<?php the_field("release_deezer",$release->ID); ?>" target="_blank"><img id="music-deezer" src="<?php bloginfo("template_directory"); ?>/img/deezer.svg"/></a>
							<?php } ?>
							<?php if (get_field("release_google_play",$release->ID)) { ?>
							<a href="<?php the_field("release_google_play",$release->ID); ?>" target="_blank"><img id="play-icon" src="<?php bloginfo("template_directory"); ?>/img/gpmusic.png"/></a>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="row show-on-small">
					<div class="col s12 centered" id="social-media-artists">
						<div class="space10"></div>
						<span class="roboto bold font18"><?php _e("COMPARTIR EN:","acmx_v1"); ?></span>
						<div class="space10"></div>
						<a href="#!" rel="<?php the_permalink($post->ID); ?>" class="roboto bold fb"><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a>
						<a href="#!" title="<?php echo $post->post_title; ?>" rel="<?php the_permalink($post->ID); ?>" class="roboto bold tw"><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a>
						<div class="space20"></div>
						<span class="roboto bold font18"><?php _e("LO QUIERO EN:","acmx_v1"); ?></span>
						<div class="space10"></div>
						<span class="roboto font14 bold buy-link">
							<?php if (get_field("release_cd",$release->ID)) { ?>
							<a href="<?=(get_field("release_cd",$release->ID)) ? get_field("release_cd",$release->ID) : '#!';?>" target="<?=(get_field("release_cd",$release->ID)) ? '_blank' : ''; ?>" class="<?=(get_field("release_cd",$release->ID)) ? 'active ' : ''; ?>magnesium-text"><?php _e("CD","acmx_v1"); ?></a>
							<?php } ?>
							<?php if (get_field("release_vinyl",$release->ID)) { ?>
							<a href="<?=(get_field("release_vinyl",$release->ID)) ? get_field("release_vinyl",$release->ID) : '#!';?>" target="<?=(get_field("release_vinyl",$release->ID)) ? '_blank' : ''; ?>" class="<?=(get_field("release_vinyl",$release->ID)) ? 'active ' : ''; ?>magnesium-text"><?php _e("VINIL","acmx_v1"); ?></a>
							<?php } ?>
							<?php if (get_field("release_digital",$release->ID)) { ?>
							<a href="<?=(get_field("release_digital",$release->ID)) ? get_field("release_digital",$release->ID) : '#!';?>" target="<?=(get_field("release_digital",$release->ID)) ? '_blank' : ''; ?>" class="<?=(get_field("release_digital",$release->ID)) ? 'active ' : ''; ?>magnesium-text"><?php _e("DIGITAL","acmx_v1"); ?></a>
							<?php } ?>
						</span>
					</div>
				</div>
				<div class="space60"></div>
			</div>
		</div>
	</div>
	
	<!-- MOST RECENT -->
	<?php 
	    //Query News
	    $args = array(
			'posts_per_page'   => 3,
			'order'			   => 'date',
			'orderby'          => 'DESC',
			'post_type'        => 'new',
			'post_status'      => 'publish',
			'suppress_filters' => false 
		);
		$posts_array = new WP_Query( $args ); 
	?>
	<div class="container-fluid" id="most-recent">
		<div class="row">
			<div class="col s12 m12 l12 brandon font48 centered">
				<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
				<div class="space40"></div>
			</div>
			<div class="container">
				<?php foreach ($posts_array->posts as $new) { $image = get_the_post_thumbnail_url( $new->ID, $size = 'full' ); ?>
				<div class="col s12 m4 l4">
					<a href="<?php echo get_permalink($new->ID); ?>">	
						<div class="card c-size black-text hoverable">
			            	<div class="card-image size-img">
								<img class="responsive-img" src="<?php echo $image; ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold titulo-busqueda font14"><?php echo $new->post_title; ?></p>
								<div class="space10"></div>
								<p class="roboto font12 justify truncate"><?php echo $new->post_excerpt; ?></p>
							</div>
			        	</div>
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- NEWSLETTER -->
		<div class="container-fluid grayer no-margin-row" id="newsletter-page">
			<div class="row">
				<div class="col s12 m12 l12 roboto bold font18 centered">
					<div class="space40"></div>
					<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
					<div class="space20"></div>
				</div>
				<div class="input-field col s12 m12 l6 offset-l3 mobile-margin60">
			        <!-- Begin MailChimp Signup Form -->
				   	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
					<style type="text/css">
						#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
					</style>
					<div id="mc_embed_signup">
						<form style="padding: 0px;" action="https://arts-crafts.us1.list-manage.com/subscribe/post?u=eda763f35af955af8a0f60dff&amp;id=94fc15a764" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						    <div id="mc_embed_signup_scroll">								
								<input style="text-indent: 10px; border: 0; height: 46px; margin-bottom: 1em; width: 100%;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php _e("Correo Electrónico","acmx_v1"); ?>" required>
							    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_eda763f35af955af8a0f60dff_94fc15a764" tabindex="-1" value=""></div>
							    <div class="clear">
								    <input style="background-color: #000; color: #fff; margin-top: -1px; border: 0; width: 100%; height: 63px; border-radius: 0px; font-family: roboto; font-size: 18px;" type="submit" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="mobile-input button">
								</div>
						    </div>
						</form>
					</div>
			        <div class="space60 hide-on-small-only"></div>
				</div>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>