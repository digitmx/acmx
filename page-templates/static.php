<?php /* Template Name: Página Estática */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); $exclude_ids = array( $post->ID ); ?>
			
			<!-- US -->
			<div class="container-fluid hero" style="background-image: url(<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>);">
				<div class="cover"></div>
				<div class="label">
					<span class="brandon bold font48 white-text title block text-label-mobile"><?php the_field("title_hero", $post->ID); ?></span>
					<div class="space10"></div>
					<span class="helvetica font14 white-text description block hide-on-small-only"><?php the_field("description_hero", $post->ID); ?></span>
				</div>
			</div>
			
			<!-- CONTACT -->
			<div class="container-fluid" id="sidebar-page">
				<div class="row">
					<div class="col s12 show-on-small">
						<span class="helvetica font14 black-text description block text-14-just"><?php the_field("description_hero", $post->ID); ?></span>
					</div>
					<div class="col s12 m3 offset-m1 l3 offset-l1">
						<?php the_field("content_sidebar", $post->ID); ?>
					</div>
					<div class="col s12 m5 offset-m1 l5 offset-l1 helvetica font14">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			
			<?php if (get_field("show_newsletter", $post->ID)) { ?>
			
			<!-- MOST RECENT -->
			<?php
				//Query News
				$args = array(
					'posts_per_page'   => 3,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'suppress_filters' => false 
				);
				$query = new WP_Query( $args );
			?>
			<div class="container-fluid grayer" id="most-recent-page">
				<div class="row">
					<div class="col s12 m12 l12 brandon font48 centered">
						<div class="space40"></div>
						<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
						<div class="space40"></div>
					</div>
					<div class="container">
						<?php while ( $query->have_posts() ) : $query->the_post(); //Proccess News ?>
						<div class="col s12 m4 l4">
							<a href="<?php the_permalink(); ?>">	
								<div class="card black-text hoverable">
					            	<div class="card-image">
										<img class="responsive-img" src="<?php echo get_the_post_thumbnail_url( $query->ID, $size = 'full' ); ?>">
									</div>
									<div class="card-content">
										<div class="roboto bold font14 block"><?php the_title(); ?></div>
										<div class="space10"></div>
										<div class="roboto light font12 block"><?php the_excerpt(); ?></div>
									</div>
					        	</div>
							</a>
						</div>
						<?php endwhile; wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
			
			<?php } ?>
			
			<?php if (get_field("show_newsletter", $post->ID)) { ?>
			
			<!-- NEWSLETTER -->
			<div class="container-fluid grayer no-margin-row" id="newsletter-page">
				<div class="row">
					<div class="col s12 m12 l12 roboto bold font18 centered">
						<div class="space40"></div>
						<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
						<div class="space20"></div>
					</div>
					<div class="input-field col s12 m6 offset-m3 l6 offset-l3 mobile-margin60 centered">
				        <!-- Begin MailChimp Signup Form -->
					   	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
						<style type="text/css">
							#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
						</style>
						<div id="mc_embed_signup">
							<form style="padding: 0px;" action="https://arts-crafts.us1.list-manage.com/subscribe/post?u=eda763f35af955af8a0f60dff&amp;id=94fc15a764" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							    <div id="mc_embed_signup_scroll">								
									<input style="text-indent: 10px; border: 0; height: 46px; margin-bottom: 1em; width: 100%;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php _e("Correo Electrónico","acmx_v1"); ?>" required>
								    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_eda763f35af955af8a0f60dff_94fc15a764" tabindex="-1" value=""></div>
								    <div class="clear">
									    <input style="background-color: #000; color: #fff; margin-top: -1px; border: 0; width: 100%; height: 63px; border-radius: 0px; font-family: roboto; font-size: 18px;" type="submit" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="mobile-input button">
									</div>
							    </div>
							</form>
						</div>
				        <div class="space60 hide-on-small-only"></div>
					</div>
				</div>
			</div>
			
			<?php } ?>
			
			<?php endwhile; ?>
			<?php endif; ?>

<?php get_footer(); ?>