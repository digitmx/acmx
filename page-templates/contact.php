<?php /* Template Name: Contact */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
		<!-- US -->
		<div class="container-fluid" id="us-contact" class="responsive-img">
			<div class="row label">
				<div class="col s12 m3 offset-m1 l3 offset-l1">
					<div class="space450"></div>
					<h1 class="brandon bold font48 white-text"><?php _e("NOSOTROS","acmx_v1"); ?></h1>
					<p class="helvetica font14 white-text">Somos un sello discográfico fundado en 2008 e integrado por entusiastas amantes de la música que creen firmemente en el desarrollo positivo de la industria musical.
					</br></br>Tenemos como misión incursionar en el atractivo y activo mercado independiente nacional y enriquecer la oferta musical a precios domésticos en nuestro país.</p>
					<div class="space120"></div>
				</div>
			</div>
		</div>

		<!-- CONTACT -->
		<div class="container-fluid" id="ls-contact">
			<div class="row contact">
				<div class="col s12 m3 offset-m1 l3 offset-l1">
					<div class="space280"></div>
					<h1 class="brandon bold font48"><?php _e("CONTACTO","acmx_v1"); ?></h1>
					<div class="space60"></div>
					<p class="helvetica light font16"><?php _e("EL. +52 (55) 6725 0698","acmx_v1"); ?></br><?php _e("EMAIL contacto@arts-crafts.com.mx","acmx_v1"); ?></p>
					<div class="space200"></div>
				</div>
				<div class="col s12 m5 l5 helvetica font14">
					<div class="space120"></div>
					<span class="bold"><?php _e("FAQ","acmx_v1"); ?></span>
					<div class="space20"></div>
					<span class="bold">•  Músicos que quieran firmar con Arts&Crafts México</span>
					<p class="light">Seguramente esto no es noticia para ustedes, pero vale la pena aclarar, es bastante raro que una banda sea firmada hoy en día por mandar un demo no solicitado a la disquera. Habiendo dicho eso, si quieren mandar su música lo pueden hacer a la dirección que encuentran hasta abajo de esta página.</br></br>
									Aunque escuchamos todo lo que nos mandan, no regresamos ningún paquete ni ofrecemos una reseña/crítica del material que nos manden. No es por mala onda, simplemete ese no es nuestro trabajo.</br></br>
									Mucha suerte a ti y a tu banda. Nunca dejen de hacer música, déjenla crecer. Aunque a veces parezca imposible, todo puede suceder. Al final del día lo único que importa es la música.</p>
					<div class="space20"></div>
					<span class="bold">•  A todos ustedes que quieran colaborar con Arts&Crafts MX</span>
					<div class="space20"></div>
					<p class="light">Es un honor que quieras colaborar con nuestro equipo de trabajo. Si quieres, manda tu curriculum o propuesta al correo que viene en la sección de contacto y revisaremos con gusto en caso que surja una oportunidad.</p>
					<div class="space20"></div>
					<span class="bold">•  Si quieres licenciar nuestra música</span>
					<p class="light">Si trabajas en cine, televisión o algo relacionado y quieres licenciar nuestra música favor de escribir a nuestro correo, en la mayoría de los casos nosotros controlamos los masters y la editora, así que puedes tratar directo con nosotros.</p>
					<div class="space100"></div>
				</div>
			</div>
		</div>
		
	<!-- MOST RECENT -->
	<div class="container-fluid" id="most-recent">
		<div class="row">
				<div class="col s12 m12 l12 brandon font48 centered">
					<div class="space40"></div>
					<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
					<div class="space40"></div>
				</div>
			<div class="container">
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_4", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">BRENDAN CANNING ANUNCIA UN NUEVO DISCO: 'HOME WRECKING YEARS'</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">El día de hoy fue estrenado el sencillo “Book It To Fresno” de Brendan Canning a través de Stereogum...</p>
							</div>
			        	</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_5", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">THE OATHS ESTRENAN VIDEO PARA "DISTINTO"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">¿Quién no ha tenido una noche que termina en día? O como dicen por ahí: “juventud, divino tesoro”. Como </p>
							</div>
						</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_6", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">YOUTH LAGOON ESTRENA UN SURREAL VIDEO PARA "ROTTEN HUMAN"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">Youth Lagoon continúa con la promoción de ‘Savage Hills Ballroom’, su más reciente producción lanzada apenas ...</p>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12 roboto bold font18 centered">
				<div class="space40"></div>
				<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
				<div class="space20"></div>
			</div>
			<div class="input-field col s12 m4 offset-m4 l4 offset-l4 centered">
		        <input id="email-most-r" type="email" class="validate">
		        <label for="email" class="roboto bold font18 black-text"><?php _e("Correo Electrónico","acmx_v1"); ?></label>
		        <input type="submit" class="roboto bold font18" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>">
		        <div class="space60"></div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>