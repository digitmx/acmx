<?php /* Template Name: API */

	//Include PHPMailer
	include_once( getcwd().'/wp-includes/class-phpmailer.php' );
	
	//Read Param Field
	$json = (isset($_POST['param'])) ? $_POST['param'] : NULL; $output = FALSE;
	$json = str_replace('\\', '', $json);

	//Check JSON
	if ($json != NULL)
	{
		//Decode Data JSON
		$json_decode = json_decode($json, true);

		//Read Action JSON
		$msg = (isset($json_decode['msg'])) ? (string)trim($json_decode['msg']) : '';

		//Read Fields JSON
		$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : array();

		//Set Wordpress Timezone
		//date_default_timezone_set("'".get_option('timezone_string')."'");
		
		// getArtists
		if ($msg == 'getArtists')
		{
			//Read Data
			$filter = (isset($fields['filter'])) ? (string)trim($fields['filter']) : '';
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '';
			$lang = (isset($fields['lang'])) ? (string)trim($fields['lang']) : '';
			$results = array();
			$html = '';
			
			//Set Error Messages
			if ($lang == 'en')
			{ 
				$error_check = 'Fields are missing.'; 
				$error_rows = 'There are no results.';
			}
			else 
			{ 
				$error_check = 'Faltan campos.';
				$error_rows = 'No hay resultados.'; 
			}
			
			//Check Values
			if ($filter && $page)
			{
				//Check Filter 
				if ($filter == 'all')
				{
					//Query Page of Artists
					$args = array(
						'posts_per_page'   => 6,
						'orderby'          => 'title',
						'order'            => 'ASC',
						'post_type'        => 'artist',
						'post_status'      => 'publish',
						'suppress_filters' => false,
						'paged'			   => $page
					);
					$query_artists = new WP_Query( $args );
				}
				else
				{
					//Query Page of Artists
					$args = array(
						'posts_per_page'   => 6,
						'orderby'          => 'title',
						'order'            => 'ASC',
						'post_type'        => 'artist',
						'post_status'      => 'publish',
						'suppress_filters' => false,
						'tax_query' => array(
							array(
								'taxonomy' => 'group',
								'field'    => 'slug',
								'terms'    => strtolower($filter),
							),
						),
						'paged'			   => $page
					);
					$query_artists = new WP_Query( $args );
				}
				
				//Check Query
				if ($query_artists->posts > 0)
				{
					//Proccess Results
					foreach ($query_artists->posts as $row_artist)
					{
						//Get Image Artist
						$image_artist = get_the_post_thumbnail_url( $row_artist->ID, $size = 'full' );
						
						//Create Element
						$results[] = array(
							'image' => $image_artist,
							'title' => $row_artist->post_title,
							'link' => get_permalink($row_artist)	
						);
						
						//Build HTML
						$html.= '<div class="item_artist">';
						$html.= '	<a href="'.get_permalink($row_artist).'">';
						$html.= '		<img class="centered-and-cropped-home-catalog" src="'.$image_artist.'"/>';
						$html.= '		<span class="artist-title roboto font16 white-text">'.$row_artist->post_title.'</span>';
						$html.= '	</a>';
						$html.= '</div>';
					}
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $results,
						'html' => $html,
						'max_pages' => $query_artists->max_num_pages
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)$error_rows,
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)$error_check,
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		// getReleases
		if ($msg == 'getReleases')
		{
			//Read Data
			$filter = (isset($fields['filter'])) ? (string)trim($fields['filter']) : '';
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '';
			$lang = (isset($fields['lang'])) ? (string)trim($fields['lang']) : '';
			$results = array();
			$html = '';
			
			//Set Error Messages
			if ($lang == 'en')
			{ 
				$error_check = 'Fields are missing.'; 
				$error_rows = 'There are no results.';
			}
			else 
			{ 
				$error_check = 'Faltan campos.';
				$error_rows = 'No hay resultados.'; 
			}
			
			//Check Values
			if ($filter && $page)
			{
				//Query Page of Artists
				$args = array(
					'post_type' 	   => 'release',
					'posts_per_page'   => 6,
					'order_by'	 	   => 'date',
					'order' 		   => 'DESC',
					'post_status'      => 'publish',
					'paged'			   => $page,
					'suppress_filters' => false 
				);
				$query_releases = new WP_Query( $args );
				
				//Check Query
				if ($query_releases->posts > 0)
				{
					//Proccess Results
					foreach ($query_releases->posts as $row_release)
					{
						//Get Image Artist
						$image_release = get_the_post_thumbnail_url( $row_release->ID, $size = 'full' );
						
						//Create Element
						$results[] = array(
							'image' => $image_release,
							'title' => $row_release->post_title,
							'link' => get_permalink($row_release)	
						);
						
						//Build HTML
						$html.= '<div class="item_artist">';
						$html.= '	<a href="'.get_permalink($row_release).'">';
						$html.= '		<img class="centered-and-cropped-home-catalog" src="'.$image_release.'"/>';
						$html.= '		<span class="artist-title roboto font16 white-text italic">'.$row_release->post_title.'</span>';
						//$html.= '<br/>';
						//$release_date = get_field("release_date", $row_release->ID);
						//$html.= substr($release_date,0,4).'/'.substr($release_date,4,2).'/'.substr($release_date,6,2).'</span>';
						$html.= '	</a>';
						$html.= '</div>';
					}
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $results,
						'html' => $html
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)$error_rows,
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)$error_check,
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		// getReleasesCatalog
		if ($msg == 'getReleasesCatalog')
		{
			//Read Data
			$filter = (isset($fields['filter'])) ? (string)trim($fields['filter']) : '';
			$page = (isset($fields['page'])) ? (string)trim($fields['page']) : '';
			$lang = (isset($fields['lang'])) ? (string)trim($fields['lang']) : '';
			$results = array();
			$html = '';
			
			//Set Error Messages
			if ($lang == 'en')
			{ 
				$error_check = 'Fields are missing.'; 
				$error_rows = 'There are no results.';
			}
			else 
			{ 
				$error_check = 'Faltan campos.';
				$error_rows = 'No hay resultados.'; 
			}
			
			//Check Values
			if ($filter && $page)
			{
				//Query Page of Artists
				$args = array(
					'post_type' 	   => 'release',
					'posts_per_page'   => 12,
					'order_by'	 	   => 'date',
					'order' 		   => 'DESC',
					'post_status'      => 'publish',
					'paged'			   => $page,
					'suppress_filters' => false 
				);
				$query_releases = new WP_Query( $args );
				
				//Check Query
				if ($query_releases->posts > 0)
				{
					//Proccess Results
					foreach ($query_releases->posts as $row_release)
					{
						//Get Image Artist
						$image_release = get_the_post_thumbnail_url( $row_release->ID, $size = 'full' );
						
						//Create Element
						$results[] = array(
							'image' => $image_release,
							'title' => $row_release->post_title,
							'link' => get_permalink($row_release)	
						);
						
						//Build HTML
						$html.= '<div class="col s4 m4 l3 item_artist_catalog mobile-padding0">';
						$html.= '	<div class="catalog-edit">';
						$html.= '      	<a href="'.get_permalink($row_release).'">';
						$html.= '	       	<img class="centered-and-cropped-catalog" src="'.$image_release.'"/>';
						$html.= '		<span class="artist-title roboto font16 white-text">';
						$html.= $row_release->post_title;
						$html.= '<br/>';
						$release_date = get_field("release_date", $row_release->ID);
						$html.= substr($release_date,0,4).'/'.substr($release_date,4,2).'/'.substr($release_date,6,2).'</span>';
						$html.= '		</a>';
						$html.= '	</div>';
						$html.= '	<div class="space10"></div>';
					    $html.= '</div>';
					}
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $results,
						'html' => $html
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)$error_rows,
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)$error_check,
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		// getProduct
		if ($msg == 'getProduct')
		{
			//Read Data
			$productid = (isset($fields['productid'])) ? (string)trim($fields['productid']) : '';
			$row = (isset($fields['row'])) ? (string)trim($fields['row']) : '1';
			
			//Check Values
			if ($productid)
			{
				//Query Product
				$product = get_post($productid);
				$html = '';
				
				//Verify Product
				if ($product)
				{
					//Proccess HTML
					$html.= '<div class="col s12 m4 offset-m1 l4 offset-l1">';
					$html.= '	<div class="space60 hide-on-large-only hide-on-med-only"></div>';
					$html.= '	<img class="responsive-img" src="' . get_the_post_thumbnail_url( $product->ID, $size = 'full' ) . '"/>';
					$html.= '</div>';
					$html.= '<div class="col s10 offset-s1 m6 l3" id="dsc">';
					$html.= '	<div class="space20 hide-on-small-only hide-on-med-only"></div>';
					$html.= '	<a href="#" rel="' . $row . '" class="light font48 black-text close_product block hide-on-large-only hide-on-med-only">X</a>';
					$html.= '	<span class="roboto bold font18">' . $product->post_title . '</span>';
					$html.= '	<p class="roboto light font12" style="text-align: justify;">' . $product->post_content . '</p>';
					$html.= '</div>';
					$html.= '<div class="col s12 m10 offset-m1 l3" id="price-s" style="text-align: center;">';
					$html.= '	<div class="space20 hide-on-small-only"></div>';
					$html.= '	<a href="#" rel="' . $row . '" class="light font48 black-text close_product block hide-on-small-only">X</a>';
					$html.= '	<span class="roboto bold font36">' . get_field("price", $product->ID) . '</span></br>';
					$html.= '	<span class="roboto font24 magnesium-text uppercase">' . get_field("type", $product->ID) . '</span>';
					$html.= '	<div class="space40"></div>';
					$html.= '	<input type="button" id="kichink-buybutton" data-id="' . get_field("product_id", $product->ID) . '" class="want-it-bt helvetica font14 inline" value="'.__("LO QUIERO","acmx_v1").'">';
					$html.= '	<div class="space100"></div>';
					$html.= '</div>';
					
					//Generate Object
					$data = array(
						'id' => $product->ID,
						'title' => $product->post_title,
						'type' => get_field("type", $product->ID),
						'cover' => get_the_post_thumbnail_url( $product->ID, $size = 'full' ),
						'price' => get_field("price", $product->ID),
						'kichink_id' => get_field("product_id", $product->ID),
						'content' => $product->post_content,
						'html' => $html
					);
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'This product id does not exists.',
						'data' => array()
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Fields are missing.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		// search
		if ($msg == 'search')
		{
			//Read Data
			$keyword = (isset($fields['keyword'])) ? (string)trim($fields['keyword']) : '';
			
			//Check Value
			if ($keyword)
			{
				//Query Artists
				$args = array(
					's'				   => $keyword,
					'posts_per_page'   => 4,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'artist',
					'post_status'      => 'publish',
					'suppress_filters' => false 
				);
				$query_artists = new WP_Query( $args );
				
				//Process Artists
				$html_artists = ''; $image_artists = ''; $count_artists = 0;
				if (count($query_artists->posts) > 0) 
				{ 
					$html_artists .='<ul id="listsearch_artists" class="listsearch roboto light font18 white-text inline">'; 
				}
				foreach ($query_artists->posts as $post_artist)
				{
					$count_artists++;
					if ($count_artists == 1) { $image_artists = get_the_post_thumbnail_url( $post_artist->ID, $size = 'full' ); }
					$html_artists .= '<li><a class="white-text uppercase" href="'.get_permalink($post_artist->ID).'">' . $post_artist->post_title . '</a></li>';
				}
				if (count($query_artists->posts) > 0) {
					$html_artists .= '<li class="underline"><a href="' . get_bloginfo("url") . '/search-artists/?keyword=' . $keyword . '" class="white-text">' . __("VER TODOS","acmx_v1"). '</a></li></ul>';
					$html_artists = '<div class="col s12 m12 l12"><span class="roboto bold font18 white-text">'.__("ARTISTAS","acmx_v1").'</span></br><div class="space20"></div></div><div class="col s4 m4 l4"><img class="responsive-img centered-and-cropped inline search-menu" src="'.$image_artists.'"/></div><div class="col s6 m6 l6">'.$html_artists.'</div>';
				}
				
				//Query Releases
				$args = array(
					's'				   => $keyword,
					'posts_per_page'   => 4,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'release',
					'post_status'      => 'publish',
					'suppress_filters' => false 
				);
				$query_releases = new WP_Query( $args );
				
				//Process Releases
				$html_releases = ''; $image_releases = ''; $count_releases = 0;
				if (count($query_releases->posts) > 0) { $html_releases .='<ul id="listsearch_releases" class="listsearch roboto light font18 white-text inline">'; }
				foreach ($query_releases->posts as $post_release)
				{
					$count_releases++;
					if ($count_releases == 1) { $image_releases = get_the_post_thumbnail_url( $post_release->ID, $size = 'full' ); }
					$html_releases .= '<li><a class="white-text uppercase" href="'.get_permalink($post_release->ID).'">' . $post_release->post_title . '</a></li>';
				}
				if (count($query_releases->posts) > 0) {
					$html_releases .= '<li class="underline"><a href="' . get_bloginfo("url") . '/search-releases/?keyword=' . $keyword . '" class="white-text">' . __("VER TODOS","acmx_v1"). '</a></li></ul>';
					$html_releases = '<div class="col s12 m12 l12"><span class="roboto bold font18 white-text">'.__("CATÁLOGO","acmx_v1").'</span></br><div class="space20"></div></div><div class="col s4 m4 l4"><img class="responsive-img centered-and-cropped inline search-menu" src="'.$image_releases.'"/></div><div class="col s6 m6 l6">'.$html_releases.'</div>';
				}
				
				//Query News
				$args = array(
					's'				   => $keyword,
					'posts_per_page'   => 4,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'suppress_filters' => false 
				);
				$query_news = new WP_Query( $args );
				
				//Process News
				$html_news = ''; $image_news = ''; $count_news = 0;
				if (count($query_news->posts) > 0) {
					$html_news .= '<div class="col s12 m10 offset-m1 l10 offset-l1">';
					$html_news .= '	<span class="roboto bold font18 white-text">'.__("NOTICIAS","acmx_v1").'</span>';
					$html_news .= '	<a class="right roboto light font18 underline white-text" href="' . get_bloginfo("url") . '/search-news/?keyword=' . $keyword . '">'.__("VER TODOS","acmx_v1").'</a>';
					$html_news .= '	<div class="space20"></div>';
					$html_news .= '</div>';
				}
				while ( $query_news->have_posts() ) : $query_news->the_post();
					$count_news++;
					$image_news = get_the_post_thumbnail_url( $query_news->ID, $size = 'full' );
					
					$html_news .= '<a href="' . get_permalink($query_news->ID) . '" class="white-text uppercase">';
					$html_news .= '<div class="';
					if ($count_news % 2) { $html_news .= 'col s12 m5offset-m1 l5 offset-l1'; } else { $html_news .= 'col s12 m5 l5'; }
					$html_news .='">';
					$html_news .= '	<div class="col s4 m4 l4">';
					$html_news .= '		<img class="responsive-img centered-and-cropped search-menu" src="' . $image_news . '"/>';
					$html_news .= '		<div class="space20"></div>';
					$html_news .= '	</div>';
					$html_news .= '	<div class="col s8 m8 l8">';
					$html_news .= '		<div class="space10"></div>';
					$html_news .= '	    <span class="roboto bold font18 block white-text uppercase">' . get_the_title() . '</span>';
					$html_news .= '	    <div class="space20"></div>';
					$html_news .= '	    <span class="helvetica light font16 magnesium-text block uppercase">' . get_the_time('F j Y') . '</span>';
					$html_news .= '	</div>';
					$html_news .= '</div>';
					$html_news .= '</a>';
				endwhile; wp_reset_postdata();
				
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'html_artists' => $html_artists,
						'image_artists' => $image_artists,
						'html_releases' => $html_releases,
						'image_releases' => $image_releases,
						'html_news' => $html_news
					)
				);
				
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Fields are missing.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
	}
	else
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Call Invalid.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}

	//Check Output
	if (!$output)
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Error.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}

?>