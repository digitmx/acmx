<?php /* Template Name: All-artists-catalog */ ?>

<?php get_header(); ?> 

	<?php create_site_menu( 'site-menu' ); ?>
			
	<!-- ALL -->
	<div class="container-fluid" id="all-art-cat">
		<div class="container">
			<!--<div class="row all">
				<div class="col s12 m12 l12 brandon font30 bold centered black-text inline"><span><?php _e("TODOS","acmx_v1"); ?></span></div>
			</div>-->	
			<div class="space40"></div>
			<?php $tab = (isset($_GET['tab'])) ? (string)trim($_GET['tab']) : 'artistas'; ?>
			
			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s4"><a <?=($tab=='artistas') ? ' class="active"' : ''; ?> href="#artistas"><span class="brandon font24 bold art-cat-play"><?php _e("ARTISTAS","acmx_v1"); ?></span></a></li>
						<li class="tab col s4"><a <?=($tab=='catalogo') ? ' class="active"' : ''; ?> href="#catalogo"><span class="brandon font24 bold art-cat-play"><?php _e("LANZAMIENTOS","acmx_v1"); ?></span></a></li>
						<li class="tab col s4"><a <?=($tab=='playlists') ? ' class="active"' : ''; ?> href="#playlists"><span class="brandon font24 bold art-cat-play"><?php _e("PLAYLISTS","acmx_v1"); ?></span></a></li>
    				</ul>
    			</div>
				<div id="artistas" class="col s12">
					<?php $artistas = get_field("artists", "option"); ?>
					<div class="space40 hide-on-med-and-down"></div>
					<div class="space20 hide-on-desktop"></div>
					<div class="row">
						<?php foreach ($artistas as $artist) { ?>
						<div class="col s6 m6 l3 item_artist_catalog mobile-padding0">
							<div class="catalog-edit">
					        	<a href="<?php echo get_permalink($artist->ID); ?>">
						        	<img class="centered-and-cropped-catalog" src="<?php echo get_the_post_thumbnail_url( $artist->ID, $size = 'full' ); ?>"/>
									<span class="artist-title-mobile artist-title roboto font16 white-text"><?php echo $artist->post_title; ?></span>
								</a>
							</div>
							<div class="space10"></div>
				        </div>
				        <?php } ?>	
					</div>
					<?php $terms = get_terms( array('taxonomy' => 'group','hide_empty' => false,  ) );?>
					<?php foreach ($terms as $term) { ?>
					<div id="artists_<?php echo $term->slug; ?>">
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12 brandon font30 bold centered black-text inline"><span><?php echo $term->name; ?></span></div>
							</div>
							<?php
								//Consulta Grupo
								$args = array(
									'posts_per_page'   => -1,
									'orderby'          => 'title',
									'order'            => 'ASC',
									'post_type'        => 'artist',
									'post_status'      => 'publish',
									'suppress_filters' => false,
									'tax_query' => array(
										array(
											'taxonomy' => 'group',
											'field'    => 'slug',
											'terms'    => strtolower($term->slug) ,
										),
									),
								);
								$query = new WP_Query( $args );
							?>
							<div class="row">
								<div class="col s12 m12 l12 centered">
									<?php foreach ($query->posts as $row_artist) { ?>
									<a href="<?php echo get_permalink($row_artist->ID); ?>">
										<span class="brandon font20 helveticacn black-text block"><?php echo $row_artist->post_title; ?></span>
									</a>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<div id="catalogo" class="col s12">
					<?php
						//Consulta Catalogo
						$args = array(
							'posts_per_page'   => 12,
							'orderby'          => 'date',
							'order'            => 'DESC',
							'post_type'        => 'release',
							'post_status'      => 'publish',
							'suppress_filters' => false
						);
						$query_releases = new WP_Query( $args );
					?>
					<input type="hidden" id="lang_site" value="<?php echo ICL_LANGUAGE_CODE; ?>">
					<input type="hidden" id="filter_catalog" value="all">
		        	<input type="hidden" id="catalog_page" value="1">
		        	<input type="hidden" id="catalog_max_page" value="<?php echo $query_releases->max_num_pages; ?>" />
		        	<input type="hidden" id="error_catalog_max_page" value="<?php _e("Ya no hay más lanzamientos por mostrar.","acmx_v1"); ?>" />
		        	<div class="space40 hide-on-med-and-down"></div>
		        	<div class="space20 hide-on-desktop"></div>
					<div class="row">
						<div class="results_catalog">
							<?php while ( $query_releases->have_posts() ) : $query_releases->the_post(); //Proccess News ?>
							<div class="col s4 m4 l3 item_artist_catalog mobile-padding0">
								<div class="catalog-edit">
						        	<a href="<?php echo get_permalink($query_releases->ID); ?>">
							        	<img class="centered-and-cropped-catalog" src="<?php echo get_the_post_thumbnail_url( $query_releases->ID, $size = 'full' ); ?>"/>
										<span class="artist-title-mobile artist-title roboto font16 white-text"><?php the_title(); ?></span>
									</a>
								</div>
								<div class="space10"></div>
					        </div>
					       <?php endwhile; wp_reset_postdata(); ?>	
						</div>
					</div>
					<div class="col12 m12 l12 centered" id="bt-all">
			        	<a href="#!" style="display: none;" class="bt-bk hoverable"><img src="<?php bloginfo("template_directory"); ?>/img/bt-back.png"></a>
			        	<a href="#!" class="bt-fr hoverable"><img src="<?php bloginfo("template_directory"); ?>/img/bt-forward-bk.png"></a>
					</div>
				</div>
				<div id="playlists" class="col s12">
					<?php $playlists = get_field("playlists", "options"); $contador_playlists = 0; $contador_contenedor_playlists = 1; ?>
		        	<input type="hidden" id="lang_site" value="<?php echo ICL_LANGUAGE_CODE; ?>">
		        	<input type="hidden" id="playlists_page" value="<?php echo $contador_contenedor_playlists; ?>">
		        	<input type="hidden" id="playlists_max_page" value="<?php echo count($playlists); ?>" />
		        	<input type="hidden" id="error_playlists_max_page" value="<?php _e("Ya no hay más playlists por mostrar.","acmx_v1"); ?>" />
		        	<div class="space40 hide-on-med-and-down"></div>
		        	<div class="space20 hide-on-desktop"></div>
		        	<div class="row">
			        	<div class="items_playlists" id="playlists_<?=$contador_contenedor_playlists;?>">
							<?php foreach ($playlists as $playlist) { $contador_playlists++; ?>
							<?php if ($contador_playlists % 7 == 0) { $contador_contenedor_playlists++; echo '</div><div class="items_playlists" id="playlists_'.$contador_contenedor_playlists.'" style="display:none;">'; $contador_playlists = 1; } ?>
							<div class="col s6 m4 l4 item_artist_catalog mobile-padding0">
								<div class="catalog-edit">
						        	<a href="<?php echo $playlist['link']; ?>">
							        	<img class="centered-and-cropped-catalog-playlists" src="<?php echo $playlist['imagen']; ?>"/>
									</a>
								</div>
								<div class="space10"></div>
					        </div>
					       <?php } ?>
			        	</div>
		        	</div>
		        	<div class="row">
						<div class="col12 m12 l12 centered" id="bt-all">
							<center>
				        	<a href="#!" style="display: none;" class="bt-bk hoverable"><img src="<?php bloginfo("template_directory"); ?>/img/bt-back.png"></a>
				        	<a href="#!" <?=(count($playlists)>1) ? ' style="display:none;"' : ''; ?> class="bt-fr hoverable"><img src="<?php bloginfo("template_directory"); ?>/img/bt-forward-bk.png"></a>
							</center>
						</div>
					</div>
				</div>
  			</div>
		</div>
	</div>

<?php get_footer(); ?>