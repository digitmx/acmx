<?php /* Template Name: Artists */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>

	<!-- Artists Bio -->
	<div class="container-fluid" id="artists-bio">
		<div class="row bio">
			<div class="space200"></div>
			<div class="col s12 m4 offset-m1 l4 offset-l1 left-aling">
				<h1 class="brandon font72 inline">REY PILA</h1>
				<p class="left-aling helvetica font14 inline">
				Rey Pila es Diego Solórzano, Rodrigo Blanco, Andrés Velasco y Miguel Hernández, aunque nació como el proyecto solista de Solórzano (ex Los Dynamite) . Su álbum homónimo debut fue grabado en el 2009 y co-producido por Diego y Paul Mahajan (TV On the Radio, Liars, Yeah Yeah Yeahs). De dicho disco se desprendieron varios sencillos exitosos, tanto en inglés como en español, como “No Longer Fun” y “No. 114”. La banda tocó sin parar por dos años en México y EUA, incluso compartiendo escenario con bandas como Interpol, Muse, TV On the Radio y Ariel Pink, y presentándose en festivales como el Vive Latino, Corona Capital y SXSW. 
				</br></br>En el 2012, la alineación actual de Rey Pila entró a los estudios DFA en Nueva York para grabar su segundo LP, The Future Sugar, con el productor Chris Coady (Beach House, Future Islands, Wavves).</p>
				<a href="#!"><i class="fa fa-facebook white-text font20 inline" aria-hidden="true" id="icon-soc-art"></i></a>
				<a href="#!"><i class="fa fa-twitter white-text font20 inline" aria-hidden="true" id="icon-soc-art"></i></a>
				<a href="#!"><i class="material-icons white-text font20 inline" id="icon-soc-art">language</i></a>
				<span class="roboto font18 black-text centered inline follow-art"><a href="#!"><i class="fa fa-spotify black-text font20 inline icon-spot-art" aria-hidden="true"></i></a><?php _e("Seguir","acmx_v1"); ?></span>
				<span class="roboto font18 black-text centered inline f-num-art">64.5 k</span>
			</div>
		</div>
		<img id="img-bio" src="<?php the_field("art_1", "option") ?>">
	</div>
	
	<!-- Artists Album -->
	<div class="container-fluid" id="disc-cal-not-press">
		<div class="space60"></div>
			<div class="container">
				<div class="row">	
					<div class="col s12 m12 l12 brandon font30 bold centered inline">
						<a href="#!"><span class="art-cat-play magnesium-text"><?php _e("DISCO","acmx_v1"); ?></span></a>
						<a href="#!"><span class="art-cat-play magnesium-text"><?php _e("CALENDARIO","acmx_v1"); ?></span></a>
						<a href="#!"><span class="art-cat-play magnesium-text"><?php _e("NOTICIAS","acmx_v1"); ?></span></a>
						<a href="#!"><span class="magnesium-text"><?php _e("PRENSA","acmx_v1"); ?></span></a>
					</div>
				</div>
			</div>
			<div class="space60"></div>
			<div class="row">
				<div class="col s6 m2 offset-m2 l2 offset-l2 right-align" id="social-media-artists">
					<div class="space100"></div>
					<span class="roboto bold font18"><?php _e("COMPARTIR EN:","acmx_v1"); ?></span>
					<div class="space10"></div>
					<a href="#!" class="roboto bold font14 fb"><?php _e("Facebook","acmx_v1"); ?></a>
					<div class="space10"></div>
					<a href="#!" class="roboto bold font14 tw"><?php _e("Twitter","acmx_v1"); ?></a>
					<div class="space10"></div>
					<a href="#!" class="roboto bold font14 pi"><?php _e("Pinterest","acmx_v1"); ?></a>
					<div class="space20"></div>
					<span class="roboto bold font18"><?php _e("LO QUIERO EN:","acmx_v1"); ?></span>
					<div class="space10"></div>
					<span class="roboto font14 bold"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" class="magnesium-text hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
				</div>
				<div class="col s6 m8 l8">
					<div class="carousel">
					    <a class="carousel-item" href="#!"><img class="responsive-img hoverable" src="<?php the_field("art_2", "option") ?>"></a>
					    <a class="carousel-item" href="#!"><img class="responsive-img hoverable" src="<?php the_field("art_3", "option") ?>"></a>
					  </div>					
				</div>
			</div>
			<div class="row">
					<div class="col s8 offset-s2 m6 offset-m3 l6 offset-l3 centered">
						<span class="brandon bold font48">THE FUTURE SUGAR</span>
						<div class="space10"></div>
						<p class="helvetica font14">2016.02.26ACMX125   | </p>
						<div class="space20"></div>
						<p class="helvetica font14">El cuarteto de la Ciudad de México estrena su segundo álbum de estudio. The Future Sugar (Arts & Crafts México, 2015), cuenta con la curaduría y producción infalibles de Julian Casablancas y Chris Coady (Beach House / Yeah Yeah Yeahs), además de la remezcla de Shawn Everett (Weezer / Lucius).
						</br></br>The Future Sugar ya está disponible en formato físico en tiendas y en formato digital en iTunes.</br>
						Además, puedes escucharlo en todos los servicios de streaming como Spotify, Apple Music, Rdioy Deezer. El lanzamiento es exclusivo para México; para el resto del mundo el disco estará disponible el 25 de septiembre.</p>
					</div>
				</div>
				<div class="space40"></div>
				<div class="container playlist-album">
				<div class="row">
					<div class="col s12 m5 offset-m2 l5 offset-l2 roboto">
						<span class="font16 magnesium-text albumn-tracks" id="album-pos" ><?php _e("ALBUM","acmx_v1"); ?></span>
						 <table class="bordered" id="table-playlist">
					        <thead  class="bold">
					          <tr>
					              <th>THE FUTURE SUGAR</th>
					          </tr>
					        </thead>
					
					        <tbody>
					          <tr>
					            <td>1 FIRE AWAY</td>
					          </tr>
					          <tr>
					            <td>2 WHITE NIGHT</td>
					          </tr>
					          <tr>
					            <td>3 SURVELLANCE CAMARE</td>
					          </tr>
					          <tr>
					            <td>4 ALEXANDER</td>
					          </tr>
					          <tr>
					            <td>5 FLASE SELF SYSTEM</td>
					          </tr>
					          <tr>
					            <td>6 THE FUTURE SUGAR</td>
					          </tr>
					          <tr>
					            <td>7 NERDS</td>
					          </tr>
					        </tbody>
					      </table>
					</div>
					<div class="col s12 m5 l5 roboto bold">
						<div class="space200"></div>
						<span><?php _e("TAMBIÉN ESCUCHANOS EN:","acmx_v1"); ?></span>
						<div class="space20"></div>
						<a href="#!"><img id="music-deezer" src="http://dev.arts-crafts.com.mx/wp-content/themes/acmx_v1/img/music.svg"/></a>
						<a href="#!"><img id="music-deezer" src="http://dev.arts-crafts.com.mx/wp-content/themes/acmx_v1/img/deezer.svg"/></a>
						<a href="#!"><img id="play-icon" src="http://dev.arts-crafts.com.mx/wp-content/themes/acmx_v1/img/play.svg"/></a>
					</div>
				</div>
			</div>
			<div class="space40"></div>
			<div class="row">
				<div class="col s8 offset-s2 m6 offset-m3 l6 offset-l3 centered">
					<p class="helvetica font14">Poco a poco, el disco fue cobrando vida con el estreno de los sencillos "Alexander", "Apex", "Blast" y recientemente con la nueva versión de "What A Nice Surprise” (con la participación de Julian Casablancas) y fueron revelando lo que hoy es una de las producciones más sólidas del año. La última probada antes del estreno, fue “Fire Away”, el nuevo corte que se estrenó en exclusiva desde Stereogum.</p>
				</div>
				<div class="col s8 offset-s2 m6 offset-m3 l6 offset-l3">
					<div class="space40"></div>
					<div class="video-container">
				        <iframe id="video-artists-bio" src="//www.youtube.com/embed/Q8TXgCzxEnw?rel=0" frameborder="0" allowfullscreen></iframe>
				    </div>
				    <div class="space80"></div>
				</div>
			</div>
	</div>
	
	<!-- MOST RECENT -->
	<div class="container-fluid" id="most-recent">
		<div class="row">
				<div class="col s12 m12 l12 brandon font48 centered">
					<div class="space40"></div>
					<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
					<div class="space40"></div>
				</div>
			<div class="container">
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_4", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">BRENDAN CANNING ANUNCIA UN NUEVO DISCO: 'HOME WRECKING YEARS'</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">El día de hoy fue estrenado el sencillo “Book It To Fresno” de Brendan Canning a través de Stereogum...</p>
							</div>
			        	</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_5", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">THE OATHS ESTRENAN VIDEO PARA "DISTINTO"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">¿Quién no ha tenido una noche que termina en día? O como dicen por ahí: “juventud, divino tesoro”. Como </p>
							</div>
						</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_6", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">YOUTH LAGOON ESTRENA UN SURREAL VIDEO PARA "ROTTEN HUMAN"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">Youth Lagoon continúa con la promoción de ‘Savage Hills Ballroom’, su más reciente producción lanzada apenas ...</p>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12 roboto bold font18 centered">
				<div class="space40"></div>
				<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
				<div class="space20"></div>
			</div>
			<div class="input-field col s12 m3 offset-m4 l3 offset-l4">
		        <input id="email-most-r" type="email" class="validate">
		        <label for="email" class="roboto bold font18 black-text"><?php _e("Correo Electrónico","acmx_v1"); ?></label>
		        <input type="submit" class="roboto bold font18" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>">
		        <div class="space60"></div>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>