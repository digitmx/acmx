<?php /* Template Name: Label-services */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
	<!-- LABEL SERVICES -->
		<div class="container-fluid" id="label-services">
			<div class="row label">
				<div class="col s12 m3 offset-m1 l3 offset-l1">
					<div class="space300"></div>
					<h1 class="brandon bold font48 white-text"><?php _e("LABEL SERVICES"); ?></h1>
					<p class="helvetica font14 white-text">Somos un sello discográfico fundado en 2008 e integrado por entusiastas amantes de la música que creen firmemente en el desarrollo positivo de la industria musical.
					</br></br>Tenemos como misión incursionar en el atractivo y activo mercado independiente nacional y enriquecer la oferta musical a precios domésticos en nuestro país.</p>
					<div class="space60"></div>
				</div>
			</div>
		</div>
		
		<!-- CONTACT -->
		<div class="container-fluid" id="ls-contact">
			<div class="row contact">
				<div class="col s12 m3 offset-m1 l3 offset-l1">
					<div class="space280"></div>
					<h1 class="brandon bold font48"><?php _e("CONTACTO","acmx_v1"); ?></h1>
					<div class="space60"></div>
					<p class="helvetica light font16"><?php _e("EL. +52 (55) 6725 0698","acmx_v1"); ?></br><?php _e("EMAIL contacto@arts-crafts.com.mx","acmx_v1"); ?></p>
					<div class="space200"></div>
				</div>
				<div class="col s12 m5 l5 helvetica font14">
					<div class="space120"></div>
					<span class="bold"><?php _e("FAQ","acmx_v1"); ?></span>
					<div class="space20"></div>
					<span class="bold">•  Músicos que quieran firmar con Arts&Crafts México</span>
					<p class="light">Seguramente esto no es noticia para ustedes, pero vale la pena aclarar, es bastante raro que una banda sea firmada hoy en día por mandar un demo no solicitado a la disquera. Habiendo dicho eso, si quieren mandar su música lo pueden hacer a la dirección que encuentran hasta abajo de esta página.</br></br>
									Aunque escuchamos todo lo que nos mandan, no regresamos ningún paquete ni ofrecemos una reseña/crítica del material que nos manden. No es por mala onda, simplemete ese no es nuestro trabajo.</br></br>
									Mucha suerte a ti y a tu banda. Nunca dejen de hacer música, déjenla crecer. Aunque a veces parezca imposible, todo puede suceder. Al final del día lo único que importa es la música.</p>
					<div class="space20"></div>
					<span class="bold">•  A todos ustedes que quieran colaborar con Arts&Crafts MX</span>
					<div class="space20"></div>
					<p class="light">Es un honor que quieras colaborar con nuestro equipo de trabajo. Si quieres, manda tu curriculum o propuesta al correo que viene en la sección de contacto y revisaremos con gusto en caso que surja una oportunidad.</p>
					<div class="space20"></div>
					<span class="bold">•  Si quieres licenciar nuestra música</span>
					<p class="light">Si trabajas en cine, televisión o algo relacionado y quieres licenciar nuestra música favor de escribir a nuestro correo, en la mayoría de los casos nosotros controlamos los masters y la editora, así que puedes tratar directo con nosotros.</p>
					<div class="space100"></div>
				</div>
			</div>
		</div>
		
	<!-- MOST RECENT -->
	<div class="container-fluid" id="most-recent">
		<div class="row">
				<div class="col s12 m12 l12 brandon font48 centered">
					<div class="space40"></div>
					<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
					<div class="space40"></div>
				</div>
			<div class="container">
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_4", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">BRENDAN CANNING ANUNCIA UN NUEVO DISCO: 'HOME WRECKING YEARS'</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">El día de hoy fue estrenado el sencillo “Book It To Fresno” de Brendan Canning a través de Stereogum...</p>
							</div>
			        	</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_5", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">THE OATHS ESTRENAN VIDEO PARA "DISTINTO"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">¿Quién no ha tenido una noche que termina en día? O como dicen por ahí: “juventud, divino tesoro”. Como </p>
							</div>
						</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_6", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">YOUTH LAGOON ESTRENA UN SURREAL VIDEO PARA "ROTTEN HUMAN"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">Youth Lagoon continúa con la promoción de ‘Savage Hills Ballroom’, su más reciente producción lanzada apenas ...</p>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12 roboto bold font18 centered">
				<div class="space40"></div>
				<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
				<div class="space20"></div>
			</div>
			<div class="input-field col s9 m3 offset-m4 l3 offset-l4">
		        <!-- Begin MailChimp Signup Form -->
			   	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
				<style type="text/css">
					#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
				</style>
				<div id="mc_embed_signup">
					<form style="padding: 0px;" action="https://arts-crafts.us1.list-manage.com/subscribe/post?u=eda763f35af955af8a0f60dff&amp;id=94fc15a764" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					    <div id="mc_embed_signup_scroll">								
							<input style="text-indent: 10px; border: 0; height: 46px; margin-bottom: 1em; width: 100%;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php _e("Correo Electrónico","acmx_v1"); ?>" required>
						    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_eda763f35af955af8a0f60dff_94fc15a764" tabindex="-1" value=""></div>
						    <div class="clear">
							    <input style="background-color: #000; color: #fff; margin-top: -1px; border: 0; width: 100%; height: 63px; border-radius: 0px; font-family: roboto; font-size: 18px;" type="submit" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="mobile-input button">
							</div>
					    </div>
					</form>
				</div>
		        <div class="space60"></div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>