<?php /* Template Name: All-events */ ?>

<?php get_header(); ?> 

		<?php create_site_menu( 'site-menu' ); ?>
			
		<!-- ALL NEWS -->
		<div class="container-fluid" id="all-events">
			<div class="row">
				<div class="col s12 m12 l12 centered brandon font30">
					<div class="space40"></div>
					<span><?php _e("CALENDARIO","acmx_v1"); ?></span>
					<div class="space40"></div>
				</div>
			</div>
			<!-- Calendario -->
			<?php $events_calendar = get_field('events_calendar', 'options'); //Leemos los Eventos ?>
			<?php $contador_eventos = 0; $events = array(); $year = date('Y'); $month = date('m'); $day = date('d'); $today = (string)$year.(string)$month.(string)$day; ?>
			<?php foreach ($events_calendar as $event) { if ((string)$event['date'] >= (string)$today) { $events[] = $event; } } // Procesamos las fechas validas ?>
			<div class="container-fluid no-margin-row" id="calendar">
				<div class="row">
				    <div class="col s12 m12 l5 offset-l1" id="date" style="position: relative;">
				        <?php foreach ($events as $row_event) { ?>
				        	<?php $contador_eventos++; if ($contador_eventos > 5) { break; } ?>
				        	<?php if ($contador_eventos == 1) { $poster = $row_event['poster']; } ?>
				        	<?php $fecha = $row_event['date']; $dia = substr($fecha, 6, 2); $mes = substr($fecha, 4, 2); $mes_string = ''; ?> 
				        	<?php
					        	switch ($mes)
					        	{
						        	case '01': $mes_string = 'ENE'; break;
						        	case '02': $mes_string = 'FEB'; break;
						        	case '03': $mes_string = 'MAR'; break;
						        	case '04': $mes_string = 'ABR'; break;
						        	case '05': $mes_string = 'MAY'; break;
						        	case '06': $mes_string = 'JUN'; break;
						        	case '07': $mes_string = 'JUL'; break;
						        	case '08': $mes_string = 'AGO'; break;
						        	case '09': $mes_string = 'SEP'; break;
						        	case '10': $mes_string = 'OCT'; break;
						        	case '11': $mes_string = 'NOV'; break;
						        	case '12': $mes_string = 'DIC'; break;
						        	default: $mes_string = 'ENE'; break;
					        	} 
					        ?>
					        <div class="divider"></div>
							<div class="row event" rel="<?php echo $row_event['poster']; ?>">
								<div class="space20"></div>
								<div class="col s2 m2 l2 roboto font24 bold centered date font17-mobile"><span><?php echo $dia; ?><br/><?php echo $mes_string; ?></span></div>
								<div class="col s5 m6 l5 roboto font24 bold uppercase block font14-mobile">
									<span id="artist"><?php echo $row_event['artist']; ?></span>
									<span class="roboto font14 bold magnesium-text block font10-mobile" id="event"><?php echo $row_event['event']; ?></span>
									<span class="roboto font14 magnesium-text block font10-mobile" id="place"><?php echo $row_event['place']; ?></span>
								</div>
								<div class="col s4 m3 l4">
									<input type="submit" id="btbuy-e" rel="<?php echo $row_event['tickets']; ?>" class="right roboto font16 inline font10-mobile-400" value="<?php _e("Comprar Boletos","acmx_v1"); ?>">
								</div>
							</div>
						<?php } ?>
						<div class="divider"></div>
						<div class="space80"></div>
			    	</div>
					<div class="col s12 m12 l6" id="artist-tour">
			        	<div class="col s12 m9 l9 hide-on-med-and-down">
					        <img class="materialboxed responsive-img poster" src="<?php echo $poster; ?>"/>
				        </div>
				        <div class="space40"></div>
			    	</div>
			    	<div class="divider"></div>
					<div class="space100"></div>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<div class="space100"></div>
				</div>
			</div>
		</div>
		
<?php get_footer(); ?>