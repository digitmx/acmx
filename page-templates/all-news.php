<?php /* Template Name: All-news */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>

			<?php
				//Query News
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'posts_per_page'   => 9,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'paged'			   => $paged,
					'suppress_filters' => false 
				);
				$query = new WP_Query( $args );
			?>
			
			<!-- ALL NEWS -->
			<div class="container-fluid" id="all-news">
				<div class="row">
					<div class="col s12 m12 l12 centered brandon font30">
						<div class="space40"></div>
						<span><?php _e("TODAS LAS NOTICIAS","acmx_v1"); ?></span>
						<div class="space40"></div>
					</div>
					<div class="container">
						<?php while ( $query->have_posts() ) : $query->the_post(); //Proccess News ?>
						<div class="col s12 m6 l4">
							<a href="<?php the_permalink(); ?>">	
								<div class="card c-size black-text hoverable">
					            	<div class="card-image size-img">
										<img class="responsive-img" src="<?php echo get_the_post_thumbnail_url( $query->ID, $size = 'full' ); ?>">
									</div>
									<div class="card-content">
										<div class="roboto bold titulo-busqueda font14 block"><?php the_title(); ?></div>
										<div class="space10"></div>
										<div class="roboto font12 block justify truncate"><?php the_excerpt(); ?></div>
									</div>
					        	</div>
							</a>
						</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="col s12 m12 l12 centered">
							<?php previous_posts_link( '&nbsp;' ); ?>
							<?php next_posts_link( '&nbsp;', $query->max_num_pages ); ?>
				        	<div class="space40"></div>
						</div>
					</div>
				</div>
			</div>

<?php get_footer(); ?>