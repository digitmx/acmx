<?php /* Template Name: All-store */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
			<!-- ALL STORE -->
			<div class="container-fluid" id="all-store">
				<div class="container">
					<div class="row all-store">
						<div class="space40"></div>
						<div class="col s12 m12 l12 brandon font30 bold centered black-text inline"><span><?php _e("TODA LA TIENDA","acmx_v1"); ?></span></div>
						<div class="space40"></div>
					</div>	
						
					<div class="row">
				    	<div class="col s12">
							<ul class="tabs centered">
								<li class="tab col s4"><a class="brandon font24 bold centered" href="#cds"><?php _e("CDS","acmx_v1"); ?></a></li>
								<li class="tab col s4"><a class="brandon font24 bold centered" href="#viniles"><?php _e("VINILES","acmx_v1"); ?></a></li>
								<li class="tab col s4"><a class="brandon font24 bold centered" href="#mercancia"><?php _e("MERCANCIA","acmx_v1"); ?></a></li>
				    		</ul>
				    	</div>
						<div id="cds" class="col s12">
							<div class="space20"></div>
							<input type="button" id="kichink-buybutton" data-id="" class="hide" value="LO QUIERO">
							<?php
								//Consultamos los CDS
								$args = array(
									'posts_per_page'   => -1,
									'orderby'          => 'date',
									'order'            => 'DESC',
									'post_type'        => 'product',
									'post_status'      => 'publish',
									'suppress_filters' => false,
									'meta_query' => array(
										array(
											'key'     => 'type',
											'value'   => 'cd',
											'compare' => '=',
										),
									),
								);
								$query = new WP_Query( $args );
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-small-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 offset-s1 m3 offset-m1 l2 <?=($contador==1) ? 'offset-l1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 5) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-small-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 m4 l2 <?=($contador==1) ? 'offset-m' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 3) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s6 m3 l2 <?=($contador==1) ? 'offset-s' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32 hide-on-small-only"></div>
									        	<?php } ?>
									        	<div class="space10 hide-on-small-only"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 2) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
						</div>
						<div id="viniles" class="col s12">
							<div class="space20"></div>
							<input type="button" id="kichink-buybutton" data-id="" class="hide" value="LO QUIERO">
							<?php
								//Consultamos los CDS
								$args = array(
									'posts_per_page'   => -1,
									'orderby'          => 'date',
									'order'            => 'DESC',
									'post_type'        => 'product',
									'post_status'      => 'publish',
									'suppress_filters' => false,
									'meta_query' => array(
										array(
											'key'     => 'type',
											'value'   => 'vinyl',
											'compare' => '=',
										),
									),
								);
								$query = new WP_Query( $args );
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-small-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 offset-s1 m3 offset-m1 l2 <?=($contador==1) ? 'offset-l1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 5) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-small-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 m3 l2 <?=($contador==1) ? 'offset-m1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 3) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 m3 l2 <?=($contador==1) ? 'offset-s1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32 hide-on-small-only"></div>
									        	<?php } ?>
									        	<div class="space10 hide-on-small-only"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 2) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
						</div>
						<div id="mercancia" class="col s12">
							<div class="space20"></div>
							<input type="button" id="kichink-buybutton" data-id="" class="hide" value="LO QUIERO">
							<?php
								//Consultamos los CDS
								$args = array(
									'posts_per_page'   => -1,
									'orderby'          => 'date',
									'order'            => 'DESC',
									'post_type'        => 'product',
									'post_status'      => 'publish',
									'suppress_filters' => false,
									'meta_query' => array(
										array(
											'key'     => 'type',
											'value'   => 'merch',
											'compare' => '=',
										),
									),
								);
								$query = new WP_Query( $args );
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-small-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 offset-s1 m3 offset-m1 l2 <?=($contador==1) ? 'offset-l1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 5) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-small-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 m3 l2 <?=($contador==1) ? 'offset-m1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32"></div>
									        	<?php } ?>
									        	<div class="space10"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 3) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
							<?php
								$contador = 0;
								$renglon_desktop = 1;
							?>
							<div class="hide-on-large-only hide-on-med-only">
								<div class="row">
									<?php foreach ($query->posts as $post) { $contador++; $status = $post->status; ?>
										<div productid="<?php echo $post->ID; ?>" rel="<?php echo $renglon_desktop; ?>" class="col s5 m3 l2 <?=($contador==1) ? 'offset-s1' : '';?> inline product_item">
											<a href="<?php echo get_permalink($post->ID); ?>">
												<?php if ($post->status != 'none') { ?>
									        	<span class="roboto font14 bold white-text inline centered block" id="tag-stock">
									        		<?php if ($post->status == 'new') { _e("NUEVO","acmx_v1"); } ?>
									        		<?php if ($post->status == 'sold') { _e("AGOTADO","acmx_v1"); } ?>
									        	</span>
									        	<?php } else { ?>
									        	<div class="space32 hide-on-small-only"></div>
									        	<?php } ?>
									        	<div class="space10 hide-on-small-only"></div>
									        	<img id="menu-des1" class="responsive-img block centered-and-cropped" src="<?php echo get_the_post_thumbnail_url( $post->ID, $size = 'full' ); ?>"/>
									        	<div class="space10"></div>
												<span class="roboto font16 bold black-text block title"><?php the_title(); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold magnesium-text block price"><?php the_field("price", $post->ID) ?></span>
												<!--
												<div class="space5"></div>
												<span class="roboto font14 magnesium-text block"><?php _e("Lo quiero en:","acmx_v1"); ?></span>
												<div class="space5"></div>
												<span class="roboto font14 bold inline"><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("CD","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("VINIL","acmx_v1"); ?></a><a href="#!" id="cd-vin-dd" class="hoverable"><?php _e("DD","acmx_v1"); ?></a></span>
												-->
												<div class="space20"></div>
											</a>
							        	</div>
							        	<?php if ($contador == 2) { echo '</div><div class="row renglon_desktop_'.$renglon_desktop.'"></div><div class="row">'; $contador = 0; $renglon_desktop++;  } ?>
						        	<?php } ?>
								</div>
								<div class="row renglon_desktop_<?php echo $renglon_desktop; ?>"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
<?php get_footer(); ?>