<?php /* Template Name: Search News */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
			<?php
				//Query News
				wp_reset_query();
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					's'				   => $_GET['keyword'],
					'posts_per_page'   => 9,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'new',
					'post_status'      => 'publish',
					'paged'			   => $paged,
					'suppress_filters' => false 
				);
				$query = new WP_Query( $args );
			?>
			
			<!-- Search -->
			<div class="container-fluid" id="all-news">
				<div class="row">
					<div class="col s12 m12 l12 centered brandon font30">
						<div class="space40"></div>
						<span><?php _e("BUSCAR NOTICIAS:","acmx_v1"); ?> <?php echo $_GET['keyword']; ?></span>
						<div class="space40"></div>
					</div>
					<div class="container">
						<?php while ( $query->have_posts() ) : $query->the_post(); //Proccess News ?>
						<div class="col s12 m6 l4">
							<a href="<?php the_permalink(); ?>">	
								<div class="card c-size black-text hoverable">
					            	<div class="card-image size-img">
										<img class="responsive-img" src="<?php echo get_the_post_thumbnail_url( $query->ID, $size = 'full' ); ?>">
									</div>
									<div class="card-content relPos">
										<span class="roboto italic white-text font14 etiqueta-busqueda">
											<?php 
												switch (get_post_type($query->ID))
												{
													case 'release': _e("Lanzamiento","acmx_v1"); break;	
													case 'new': _e("Noticia","acmx_v1"); break;	
													case 'artist': _e("Artista","acmx_v1"); break;	
													case 'product': _e("Producto","acmx_v1"); break;	
												} 
											?>
										</span>
										<div class="roboto bold font14 block titulo-busqueda"><?php the_title(); ?></div>
										<div class="space10"></div>
										<div class="roboto light font12 block justify parrafo-busqueda"><?php the_excerpt(); ?></div>
									</div>
					        	</div>
							</a>
						</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="col s12 m12 l12 centered">
							<?php previous_posts_link( '&nbsp;' ); ?>
							<?php next_posts_link( '&nbsp;', $query->max_num_pages ); ?>
				        	<div class="space40"></div>
						</div>
					</div>
				</div>
			</div>

<?php get_footer(); ?>