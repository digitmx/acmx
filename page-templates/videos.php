<?php /* Template Name: Videos */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
		<!-- VIDEOS -->
	<div class="container-fluid" id="all-videos">	
		<div class="row all">
			<div class="col s12 m12 l12 brandon font30 bold centered black-text"><span><?php _e("TODOS LOS VIDEOS","acmx_v1"); ?></span></div>
		</div>	
			<div class="space40"></div>
			<div class="row">	
				<div class="col s12 m12 l12 brandon font24 bold centered">
					<a href="#!"><span class="art-cat-play magnesium-text"><?php _e("CLASICOS","acmx_v1"); ?></span></a>
					<a href="#!"><span class="magnesium-text"><?php _e("LANZAMIENTOS","acmx_v1"); ?></span></a>
					<div class="space40"></div>
				</div>
			</div>
		<div class="container">
			<div class="row all-videos">
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/MPipMQvKgKk" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/Py2KOyrtq6o" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/CGQmN76FAGc" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/mNhW7pqCJY8" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/KJyjzHIgqr4" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/VoQTtU9qDRY" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/MPipMQvKgKk" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/Py2KOyrtq6o" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/CGQmN76FAGc" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/mNhW7pqCJY8" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/KJyjzHIgqr4" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s6 m6 l4 videos-padding0">
					<iframe src="https://www.youtube.com/embed/VoQTtU9qDRY" frameborder="0" allowfullscreen></iframe>
					<div class="space20"></div>
				</div>
				<div class="col s12 m12 l12 centered">
		        	<a href="#!" class="bt-bk hoverable"><img src="http://dev.arts-crafts.com.mx/wp-content/themes/acmx_v1/img/bt-back.png"></a>
		        	<a href="#!" class="bt-fr hoverable"><img src="http://dev.arts-crafts.com.mx/wp-content/themes/acmx_v1/img/bt-forward-bk.png"></a>
	        	<div class="space40"></div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>