<?php /* Template Name: Single-news */ ?>

<?php get_header(); ?> 

			<?php create_site_menu( 'site-menu' ); ?>
			
			<!-- SINGLE NEWS -->
			<div class="container-fluid" id="single-news">
				<div class="row single-news">
					<div class="col s12 m8 offset-m2 l8 offset-l2 centered">
						<div class="space60"></div>
						<span class="brandon font40">JAPANDROIDS ESTRENA NUEVO SENCILLO</br>DE CARA AL ESTRENO DE SU DISCO</span>
						<div class="space20"></div>
						<span class="helvetica light font18">MARCH 13 2017</span>
						<div class="space20"></div>
					</div>
					<div class="col s10 offset-s1 m10 offset-m1 l10 offset-l1">
						<img class="responsive-img" src="<?php the_field("art_7", "option") ?>"/>
						<div class="space60"></div>
					</div>
					<div class="col s12 m2 offset-m1 l2 offset-l1 right-align">
						<span class="roboto bold font18"><?php _e("COMPARTIR EN:","acmx_v1"); ?></span>
						<div class="space20"></div>
						<a href="#!" class="roboto bold font14 fb"><?php _e("Facebook","acmx_v1"); ?></a>
						<div class="space10"></div>
						<a href="#!" class="roboto bold font14 tw"><?php _e("Twitter","acmx_v1"); ?></a>
						<div class="space10"></div>
						<a href="#!" class="roboto bold font14 pi"><?php _e("Pinterest","acmx_v1"); ?></a>
						<div class="space20"></div>
					</div>
					<div class="col s12 m6 offset-m1 l6 offset-l1 helvetica font14">
						<span>Después de tocar el último de más de 200 shows en más de 40 países como parte de la gira de su disco aclamado por la crítica, Celebration Rock, Japandroids se tomó un merecido descanso para recuperarse después del que fuera su último show en noviembre de 2013, en Buenos Aires, Argentina.</span>
						<div class="space20"></div>
						<p>Luego de esa pausa, la banda compartió su regreso hace un par de meses, cuando en sus redes sociales publicaran algunas fechas al rededor del globo. Así, la emoción inició hasta culminar el pasado 25 de noviembre en un emotivo show en el Cardura de la CDMX.</p>
						<div class="space20"></div>
						<p>Un recorrido por lo mejor de sus primeros dos discos, hasta llegar a algunos destellos de lo que escucharemos el próximo enero con la llegada de Near To The Wild Heart Of Life, su tercer y muy esperado álbum de estudio.</p>
						<div class="space80"></div>
					</div>
				</div>
			</div>
			
	<!-- MOST RECENT -->
	<div class="container-fluid" id="most-recent">
		<div class="row">
				<div class="col s12 m12 l12 brandon font48 centered">
					<div class="space40"></div>
					<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
					<div class="space40"></div>
				</div>
			<div class="container">
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_4", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">BRENDAN CANNING ANUNCIA UN NUEVO DISCO: 'HOME WRECKING YEARS'</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">El día de hoy fue estrenado el sencillo “Book It To Fresno” de Brendan Canning a través de Stereogum...</p>
							</div>
			        	</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_5", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">THE OATHS ESTRENAN VIDEO PARA "DISTINTO"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">¿Quién no ha tenido una noche que termina en día? O como dicen por ahí: “juventud, divino tesoro”. Como </p>
							</div>
						</div>
					</a>
				</div>
				<div class="col s12 m6 l4">
					<a href="#!">	
						<div class="card black-text hoverable">
			            	<div class="card-image">
								<img class="responsive-img" src="<?php the_field("art_6", "option") ?>">
							</div>
							<div class="card-content">
								<p class="roboto bold font14">YOUTH LAGOON ESTRENA UN SURREAL VIDEO PARA "ROTTEN HUMAN"</p>
								<div class="space10"></div>
								<p class="roboto light font12 truncate">Youth Lagoon continúa con la promoción de ‘Savage Hills Ballroom’, su más reciente producción lanzada apenas ...</p>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12 roboto bold font18 centered">
				<div class="space40"></div>
				<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
				<div class="space20"></div>
			</div>
			<div class="input-field col s12 m3 offset-m4 l3 offset-l4">
		        <input id="email-most-r" type="email" class="validate">
		        <label for="email" class="roboto bold font18 black-text"><?php _e("Correo Electrónico","acmx_v1"); ?></label>
		        <input type="submit" class="roboto bold font18" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>">
		        <div class="space60"></div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>