<?php get_header(); ?> 

			<?php if (have_posts()) : while (have_posts()) : the_post(); $exclude_ids = array( $post->ID ); ?>
			
			<?php create_site_menu( 'site-menu' ); ?>
			
			<!-- ALL STORE -->
			<?php $exclude_ids = array( $post->ID ); ?>
			<div class="container-fluid" id="all-store">
				<div class="container">
					<div class="row all-store">
						<div class="space40"></div>
						<div class="col s12 m12 l12 brandon font30 bold centered black-text inline uppercase"><span><?php the_title(); ?></span></div>
						<div class="space40"></div>
					</div>
		      	
			      	<div class="row">
				      	<div class="col s12 m6 l8">
					      	<?php $gallery = get_field("gallery", $post->ID); ?>
					      	<div class="slider">
						      	<ul class="slides">
							    	<?php foreach ($gallery as $item) { ?>
									<li>
										<img class="responsive-img" src="<?php echo $item['url']; ?>">
									</li> 
									<?php } ?>
							    </ul>			      	
							</div>
				      	</div>
				      	<div class="col s12 m6 l4">
					      	<div class="space40 hide-on-small-only"></div>
					      	<span class="brandon font48 block"><?php the_field("price", $post->ID); ?></span>
					      	<span class="helvetica font18 block magnesium-text"><?php the_content(); ?></span>
					      	<div class="space20"></div>
					      	<div class="space20 hide-on-small-only"></div>
					      	<div class="centered">
						      	<a href="#" id="kichink-buybutton" data-id="<?php the_field("product_id", $post->ID); ?>" class="helvetica font14 btn black"><?php _e("COMPRAR","acmx_v1"); ?></a>
					      	</div>
				      	</div>
			      	</div>
				</div>
			</div>
			
			<?php endwhile; ?>
			<?php endif; ?>
			
			<!-- MOST RECENT -->
			<?php 
			    //Query News
			    $args = array(
					'posts_per_page'   => 3,
					'order'			   => 'date',
					'orderby'          => 'DESC',
					'post_type'        => 'product',
					'post_status'      => 'publish',
					'post__not_in'	   => $exclude_ids,
					'suppress_filters' => false 
				);
				$posts_array = new WP_Query( $args ); 
			?>
			<div class="container-fluid" id="most-recent">
				<div class="row">
						<div class="col s12 m12 l12 brandon font48 centered">
							<div class="space40 hide-on-small-only"></div>
							<span><?php _e("LO MÁS RECIENTE","acmx_v1"); ?></span>
							<div class="space40"></div>
						</div>
					<div class="container">
						<?php foreach ($posts_array->posts as $new) { $image = get_the_post_thumbnail_url( $new->ID, $size = 'full' ); ?>
						<div class="col s12 m4 l4">
							<a href="<?php echo get_permalink($new->ID); ?>">	
								<div class="card c-size black-text hoverable">
					            	<div class="card-image size-img">
										<img class="responsive-img" src="<?php echo $image; ?>">
									</div>
									<div class="card-content">
										<p class="roboto bold titulo-busqueda font14"><?php echo $new->post_title; ?></p>
									</div>
					        	</div>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
				<!-- NEWSLETTER -->
				<div class="container-fluid grayer no-margin-row" id="newsletter-page">
					<div class="row">
						<div class="col s12 m12 l12 roboto bold font18 centered">
							<div class="space40"></div>
							<span><?php _e("NEWSLETTER","acmx_v1"); ?></span>
							<div class="space20"></div>
						</div>
						<div class="input-field col s12 m12 l6 offset-l3 mobile-margin60">
					        <!-- Begin MailChimp Signup Form -->
						   	<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
							<style type="text/css">
								#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
							</style>
							<div id="mc_embed_signup">
								<form style="padding: 0px;" action="https://arts-crafts.us1.list-manage.com/subscribe/post?u=eda763f35af955af8a0f60dff&amp;id=94fc15a764" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								    <div id="mc_embed_signup_scroll">								
										<input style="text-indent: 10px; border: 0; height: 46px; margin-bottom: 1em; width: 100%;" type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php _e("Correo Electrónico","acmx_v1"); ?>" required>
									    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_eda763f35af955af8a0f60dff_94fc15a764" tabindex="-1" value=""></div>
									    <div class="clear">
										    <input style="background-color: #000; color: #fff; margin-top: -1px; border: 0; width: 100%; height: 63px; border-radius: 0px; font-family: roboto; font-size: 18px;" type="submit" value="<?php _e("SUSCRÍBETE","acmx_v1"); ?>" name="subscribe" id="mc-embedded-subscribe" class="mobile-input button">
										</div>
								    </div>
								</form>
							</div>
					        <div class="space60 hide-on-small-only"></div>
						</div>
					</div>
				</div>
			</div>
			
<?php get_footer(); ?>